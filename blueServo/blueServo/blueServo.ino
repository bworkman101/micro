#include <Servo.h>
#include <SoftwareSerial.h>

SoftwareSerial portBlue(2, 3); // rx, tx

Servo servo;

boolean ready = false;
int READY_BYTES = 255;

void setup() {
  portBlue.begin(38400);
  
  pinMode(13, OUTPUT); // the led
  digitalWrite(13, LOW); // turn of led initially
  
  servo.attach(7);
  servo.write(90);
}

void loop () {  
  if (portBlue.available() > 0) {
    int bytes = portBlue.read();
    if (!ready && bytes == READY_BYTES) {
      ready = true;
    }
    if (ready) {
      servo.write(bytes);
      portBlue.println("rotating " + String(bytes, DEC) + " degrees");
    }
  }
}
