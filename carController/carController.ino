#include <Servo.h>
#include <SoftwareSerial.h>

Servo servo;

// ------------------------------- Remote Command Configurations -------------------------
  int STEER_CMD = 1;
  int MOTOR_CMD_FWD = 2;
  int MOTOR_CMD_REV = 3;
  int MOTOR_CMD_STOP = 4;
  int MOTOR_CMD_SPEED = 5;
  int currentCmd = 0;
// ---------------------------------------------------------------------------------------

// ------------------------------- Controller Pin Configurations -------------------------
  // Motor  Driver Pins
  int PIN_MOTOR_ON_OFF = 6;
  int PIN_MOTOR_ONE_A = 7;
  int PIN_MOTOR_TWO_A = 8;
  
  // Steering Servo Pin
  int PIN_STEER = 5;
  
  // IR Sensor Pins
  int PIN_IR = 9;
  int PIN_IR_SERVO = 4;
  
  // Blue Tooth Pins
  int PIN_BT_RX = 2;
  int PIN_BT_TX = 3;
// ---------------------------------------------------------------------------------------

SoftwareSerial portBlue(PIN_BT_RX, PIN_BT_TX);

void setup()
{
  portBlue.begin(38400);
  
  servo.attach(PIN_STEER);
  
  pinMode(PIN_MOTOR_ON_OFF, OUTPUT);
  pinMode(PIN_MOTOR_ONE_A, OUTPUT);
  pinMode(PIN_MOTOR_TWO_A, OUTPUT);
  
  analogWrite(PIN_MOTOR_ON_OFF, 255);
}

void loop()
{
  /**
  * Reads a sequence of commands and values.
  * The first byte should be a command, and the second
  * is the value of that command.
  */
  if (Serial.available() > 0) {
    int aByte = Serial.read();
    portBlue.println("command recieved " + String(aByte, DEC) + " current command " + String(currentCmd, DEC));
    portBlue.println("");
    if (currentCmd == 0) {
      if (aByte == MOTOR_CMD_FWD) {
        digitalWrite(PIN_MOTOR_ONE_A, LOW);
        digitalWrite(PIN_MOTOR_TWO_A, HIGH);
      }
      else if (aByte == MOTOR_CMD_REV) {
        digitalWrite(PIN_MOTOR_ONE_A, HIGH);
        digitalWrite(PIN_MOTOR_TWO_A, LOW);
      }
      else if (aByte == MOTOR_CMD_STOP) {
        digitalWrite(PIN_MOTOR_ONE_A, LOW);
        digitalWrite(PIN_MOTOR_TWO_A, LOW);
      }
      else {
        currentCmd = aByte;
      }
    }
    else {
      if (currentCmd == STEER_CMD) {
        servo.write(aByte);
        portBlue.println("steer command " + String(aByte, DEC));
      }
      //else if (currentCmd == MOTOR_CMD_SPEED) {
      //  analogWrite(PIN_ON_OFF, aByte);
      //}
      currentCmd = 0;
    }
  }
}
