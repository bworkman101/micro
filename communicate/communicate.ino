
#include <Servo.h>

Servo servo;

void setup() {                
  Serial.begin(9600);
  servo.attach(7);
  servo.write(90);
}

void loop() {
  if (Serial.available() > 0) {
    int bytes = Serial.read();
    Serial.println("rotating " + String(bytes, DEC) + " degrees");
    servo.write(bytes);
  }
}
