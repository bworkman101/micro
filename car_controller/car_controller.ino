// #include <ServoTimer2.h>
#include <Servo.h>
#include <SoftwareSerial.h>

#define RxD 2
#define TxD 3

SoftwareSerial blue(RxD,TxD);

int pb0 = 8;  // servo 1
int pb1 = 9;  // servo 3
int pd4 = 4;  // motor 1
int pd6 = 6;  // motor 2
int pc3 = 3;  // IR scanner

Servo servo1;
Servo servo3;

void setup() 
{ 
  
  pinMode(RxD, INPUT);
  pinMode(TxD, OUTPUT);
  
  pinMode(pd4, OUTPUT);
  pinMode(pd6, OUTPUT);

  servo1.attach(pb0);
  servo3.attach(pb1);

  servo1.write(90);
  servo3.write(90);
}

// Bluetooth control variables
boolean started = false;
boolean inquiring = false;

// Receiver control variables
char rcvd[] = "    ";
short rcvdCnt = 0;
boolean ctrl = false;

// Scanner variables
String scanStr = "";
byte scanCtr = 0;
boolean irScan = false;
byte scanAngle = 0;
byte scanDir = 3; // scan direction increment

void loop()
{ 
 
  if(!started) {
    setupBlue();
  } else if (started && !inquiring) {
    inquire();
  }

  blue.overflow(); // not sure what to do if this overflows, but we do need to clear the overflow bit

  while (blue.available() > 0) {
    char ch = blue.read();
    if (ch == 'z') {
      if (!ctrl) {
        ctrl = true;
      }
      else if (ctrl && rcvdCnt == 4) {
        // everything received
        int val = charToNum(rcvd[1]) * 100 + charToNum(rcvd[2]) * 10 + charToNum(rcvd[3]);
        control(rcvd[0], val);
        ctrl = false;
        rcvdCnt = 0;
      }
      else {
        // if received is 0, this could be zz combo
        if (rcvdCnt > 0) {
          // noise, reset everything
          ctrl = false;
          rcvdCnt = 0;
        }
      }
    }
    else if (ctrl) {
      if (rcvdCnt == 4) {
        // noise
        ctrl = false;
        rcvdCnt = 0;
      }
      else {
        rcvd[rcvdCnt] = ch;
      }
      rcvdCnt++;
    }
  }
  
  if (irScan) {
    if (scanCtr < 20) {
      scanStr += "r" + String(analogRead(pc3), DEC) + "z";
      scanCtr += 1;
    }
    else {
      servo3.detach();
      
      blue.print("a" + String(scanAngle, DEC) + "z" + scanStr);
      blue.flush();
      
      servo3.attach(pb1);
      servo3.write(scanAngle);
      delay(120);
      
      if (scanAngle <= 30 || scanAngle >= 150) {
        scanDir = scanDir * -1;  // switch the scanning direction
      }
      
      scanAngle += scanDir;
      scanStr = "";
      scanCtr = 0;
    }
  }
    
}

void control(char ctrlChar, int val) {
  switch (ctrlChar) {
    case 'a': {
      servo1.write(val);
      break;
    }
    case 'b': {
      servo3.attach(pb1);
      servo3.write(val);
      break;
    }
    case 'm': { 
      digitalWrite(pd4, val);
      break;
    }
    case 'n': { 
      digitalWrite(pd6, val);
      break;
    }
    case 'i': {
      irScan = val == 0 ? false : true;
      if (irScan) {
        servo1.detach();
        servo3.attach(pb1);
        scanAngle = 45;
        servo3.write(scanAngle);
      }
      else {
        servo1.attach(pb0);
        servo3.detach();
      }
      break;
    }
  }
}

int charToNum(char ch) {
  switch(ch) {
    case '0': return 0;
    case '1': return 1;
    case '2': return 2;
    case '3': return 3;
    case '4': return 4;
    case '5': return 5;
    case '6': return 6;
    case '7': return 7;
    case '8': return 8;
    case '9': return 9;
    default: return 0;
  }
}

void setupBlue()
{
  blue.begin(38400); //Set BluetoothBee BaudRate to default baud rate 38400
  delay(1000);
  sendBlueCmd("\r\n+STWMOD=0\r\n");
  sendBlueCmd("\r\n+STNA=speedy\r\n");
  sendBlueCmd("\r\n+STAUTO=0\r\n");
  sendBlueCmd("\r\n+STOAUT=1\r\n");
  sendBlueCmd("\r\n +STPIN=0000\r\n");
  started = true;
  delay(2000);
}

void inquire() {
  sendBlueCmd("\r\n+INQ=1\r\n");
  inquiring = true;
}  

void sendBlueCmd(char command[])
{
    blue.print(command);
    blue.flush();
    CheckOK();   
}

void CheckOK()
{
 char a,b;
 while(1)
 {
   if(blue.available())
   {
     a = blue.read();

     if('O' == a)
     {
       // Wait for next character K. available() is required in some cases, as K is not immediately available.
       while(blue.available()) 
       {
         b = blue.read();
         break;
       }
       if('K' == b)
       {
         break;
       }
     }
   }
 }

 while( (a = blue.read()) != -1)
 {
   //Wait until all other response chars are received
 }
}

// int degToMillis(int deg) {
//  return (deg * 5.556) + 1000;
// }
