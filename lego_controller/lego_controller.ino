#include <SoftwareSerial.h>
#include <Servo.h>

int bluetoothTx = 4;  // TX-O pin of bluetooth mate, Arduino D2
int bluetoothRx = 5;  // RX-I pin of bluetooth mate, Arduino D3

int m_pwm = 3;
int m_fw = 8;
int m_bk = 13;

SoftwareSerial bluetooth(bluetoothTx, bluetoothRx);
Servo servo1;

int servoPort = 6;

void setup() {
  pinMode(m_pwm, OUTPUT);
  analogWrite(m_pwm, 0);
   
  // slow it down for the clock
  delay(2000);
  bluetooth.begin(115200);
  bluetooth.print("$");
  bluetooth.print("$");
  bluetooth.print("$");
  delay(100);
  bluetooth.println("U,9600,N");
  bluetooth.begin(9600);
  delay(100);
  bluetooth.print("-");
  bluetooth.print("-");
  bluetooth.print("-");
  delay(100);

  pinMode(m_fw, OUTPUT);
  pinMode(m_bk, OUTPUT);
  servo1.attach(servoPort);
  servo1.write(90);
}

char rcvd[] = "    ";
short rcvdCnt = 0;
boolean ctrl = false;

void loop() {
  // put your main code here, to run repeatedly:

  bluetooth.overflow(); // not sure what to do if this overflows, but we do need to clear the overflow bit

  while (bluetooth.available() > 0) {
    servo1.detach();
    char ch = bluetooth.read();
    if (ch == 'z') {
      if (!ctrl) {
        ctrl = true;
      }
      else if (ctrl && rcvdCnt == 4) {
        // everything received
        int val = charToNum(rcvd[1]) * 100 + charToNum(rcvd[2]) * 10 + charToNum(rcvd[3]);
//        servo1.attach(servoPort);
        control(rcvd[0], val);
        ctrl = false;
        rcvdCnt = 0;
      }
      else {
        // if received is 0, this could be zz combo
        if (rcvdCnt > 0) {
          // noise, reset everything
          ctrl = false;
          rcvdCnt = 0;
        }
      }
    }
    else if (ctrl) {
      if (rcvdCnt == 4) {
        // noise
        ctrl = false;
        rcvdCnt = 0;
      }
      else {
        rcvd[rcvdCnt] = ch;
      }
      rcvdCnt++;
    }
  }

  servo1.attach(servoPort);
}

void control(char ctrlChar, int val) {
  switch (ctrlChar) {
    case 'b': {
      servo1.write(val);
      break;
    }
    case 'm': {
      if (val < 253)
      {
        digitalWrite(m_bk, HIGH);
        digitalWrite(m_fw, LOW);
        if (255 - val > 50)  
        {
          analogWrite(m_pwm, 255 - val);
        }
        else
        {
          analogWrite(m_pwm, 0);
        }
//        analogWrite(m_pwm, 0);
      }
      else
      {
        digitalWrite(m_bk, LOW);
        digitalWrite(m_fw, HIGH);
        if (val - 253 > 50)  
        {
          analogWrite(m_pwm, val - 253);
        }
        else
        {
          analogWrite(m_pwm, 0);
        }
      }
      break;
    }
  }
}

int charToNum(char ch) {
  switch(ch) {
    case '0': return 0;
    case '1': return 1;
    case '2': return 2;
    case '3': return 3;
    case '4': return 4;
    case '5': return 5;
    case '6': return 6;
    case '7': return 7;
    case '8': return 8;
    case '9': return 9;
    default: return 0;
  }
}

