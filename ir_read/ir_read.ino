#include <Servo.h>

Servo servo;
int sensorPin = 9;
int servoPin = 4;
byte b[10];
int pos = 90;
int dir = 1;
int minAngle = 1;
int maxAngle = 180;

void setup() {
  Serial.begin(9600);
  servo.attach(servoPin);
  servo.write(pos);
}

void loop() {
  
  // read the value from the sensor:
  int sensorValue = analogRead(sensorPin);
  
  if (sensorValue == 0) {
    sensorValue = 1; // avoid using serial break
  }
  
  long time = millis();
  
  // offset by 7 degrees to makeup for the lagging ir reads
  if (dir == -1) {
    integerToBytes(sensorValue, pos + 7, time, b);
  } else {
    integerToBytes(sensorValue, pos - 7, time, b);
  }
  
  Serial.write(1); // set serial break
  Serial.write(2);
  Serial.write(3);
  
  for (int i=0; i<6; ++i) {
    Serial.write((int )b[i]);
  }
  
  // move servo
  if (pos == maxAngle) {
    dir = -1;
  }
  
  else if (pos == minAngle) {
    dir = 1;
  }
  pos = pos + dir;
  servo.write(pos);
  delay(1);
}

void integerToBytes(long val, int pos, long time, byte b[10]) {
  b[0] = (byte) (val >> 24);
  b[1] = (byte) (val >> 16);
  b[2] = (byte) (val >> 8);
  b[3] = (byte) (val /*>> 0*/);
  b[4] = (byte) (pos >> 8);
  b[5] = (byte) (pos);
  b[6] = (byte) (time >> 24);
  b[7] = (byte) (time >> 16);
  b[8] = (byte) (time >> 8);
  b[9] = (byte) (time /*>> 0*/);
}
