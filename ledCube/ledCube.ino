#include <Servo.h>

Servo servo;

int level[] = {3,5,6,9};
int column[] = {7,8,10,11};
int g1 = 12;
int g2 = 13;
char serialBuff[64];
int levelOn[4];
int bytes16[16][4];

void setup() {
  Serial.begin(9600);
  
  pinMode(level[0], OUTPUT);
  pinMode(level[1], OUTPUT);
  pinMode(level[2], OUTPUT);
  pinMode(level[3], OUTPUT);
  pinMode(column[0], OUTPUT);
  pinMode(column[1], OUTPUT);
  pinMode(column[2], OUTPUT);
  pinMode(column[3], OUTPUT);
  pinMode(g1, OUTPUT);
  pinMode(g2, OUTPUT);
  digitalWrite(g1, LOW);
  digitalWrite(g2, LOW);

  for (int i = 0; i < 64; i++) {
    serialBuff[i] = 0;
  }
  
  for (int i = 0; i < 16; i++) {
    bytes16[i][0] = (i & 1);
    bytes16[i][1] = (i & 2) >> 1;
    bytes16[i][2] = (i & 4) >> 2;
    bytes16[i][3] = (i & 8) >> 3;
  }

}

void loop() {

  if (Serial.available() > 0) {
    Serial.readBytes(serialBuff, 64);
  }
  
  for (int i = 0; i < 4; i++) {
    for (int j = 0; j < 16; j++) {
      ctrlLight(level[i], 0);
      if (serialBuff[(i * 16) + j] == 1)
      {
        ctrlLight(column[0], bytes16[j][0]);
        ctrlLight(column[1], bytes16[j][1]);
        ctrlLight(column[2], bytes16[j][2]);
        ctrlLight(column[3], bytes16[j][3]);
        ctrlLight(level[i], serialBuff[(i * 16) + j]);
        delayMicroseconds(100);
      }
    }
    ctrlLight(level[i], 0);
  }

}

void ctrlLight(int pin, byte isOn) {
  if (isOn == 1) {
    digitalWrite(pin, HIGH);
  } 
  else {
    digitalWrite(pin, LOW);
  }
}
