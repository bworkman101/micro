#include <Servo.h> 

Servo lightServo;

int servoPin = 6;
int switchPin = 4;

void setup() 
{ 
  Serial.begin(9600);
  lightServo.attach(servoPin);
  pinMode(switchPin, OUTPUT);
  digitalWrite(switchPin, LOW);
}

void loop()
{
  if (Serial.available() > 0) {
    char ch = Serial.read();
    if (ch == 'G') {
      lightServo.write(180);
    }
    if (ch == 'R') {
      lightServo.write(90);      
    }
    if (ch == 'Y') {
      lightServo.write(1);
    }
    if (ch == 'O') {
      digitalWrite(switchPin, HIGH);
    }
    if (ch == 'F') {
      digitalWrite(switchPin, LOW);
    }
  }
}
