#include <Servo.h>

Servo esc;
int throttlePin = 6;

void setup() {
  esc.attach(throttlePin);
}

void loop() {
  int throttle = analogRead(A5);
  throttle = map(throttle, 0, 1023, 0, 179);
  esc.write(throttle);
}
