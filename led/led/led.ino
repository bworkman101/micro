int red = 6;                 // LED connected to digital pin 13
int green = 5;
int blue = 3;

int readCount = 0;

void setup()
{
  Serial.begin(9600);
  
  pinMode(red, OUTPUT);
  pinMode(green, OUTPUT);
  pinMode(blue, OUTPUT);
  digitalWrite(red, HIGH);
  digitalWrite(green, HIGH);
  digitalWrite(blue, HIGH);
}

void loop()
{
  if (Serial.available()) {
    int color = Serial.read();
    readCount++;
    if (readCount == 1) {
      analogWrite(red, color);
      Serial.println("writing red " + String(red, DEC));
    }
    if (readCount == 2) {
      analogWrite(green, color);
      Serial.println("writing green " + String(green, DEC));
    }
    if (readCount == 3) {
      analogWrite(blue, color);
      Serial.println("writing blue " + String(blue, DEC));
      readCount = 0;
    }
    Serial.flush();
  }
}
