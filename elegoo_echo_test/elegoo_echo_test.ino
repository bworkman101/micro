int Echo = A4;  
int Trig = A5;

int ENL=5;
int ENR=11;

int RB=6;
int RF=7;
int LF=8;
int LB=9;

void setup() {
  Serial.begin(9600);
  
  pinMode(Echo, INPUT);    
  pinMode(Trig, OUTPUT);
  pinMode(LF, OUTPUT);
  pinMode(LB, OUTPUT);
  pinMode(RB, OUTPUT);
  pinMode(RF, OUTPUT);

  pinMode(ENL, OUTPUT);
  pinMode(ENR, OUTPUT);
  
  delay(3000);
  Serial.write("car ready");

  digitalWrite(ENL,HIGH);  
  digitalWrite(ENR,HIGH);
}

void loop() {
  int dist = distanceTest();

  Serial.print(dist);

  if (0 < dist && 45 < dist) {
    Serial.println("    drive");
    analogWrite(LF, 255);
    analogWrite(LB, 0);
    analogWrite(RF, 255);
    analogWrite(RB, 0);
  } else {
    Serial.println("    stop");
    analogWrite(LF, 0);
    analogWrite(LB, 0);
    analogWrite(RF, 0);
    analogWrite(RB, 0);
  }
  
//  Serial.print(dist);
//  Serial.println("-cm ");
}

int distanceTest()   
{
  digitalWrite(Trig, LOW);   
  delayMicroseconds(2);
  digitalWrite(Trig, HIGH);  
  delayMicroseconds(10);
  digitalWrite(Trig, LOW);   
  int Fdistance = pulseIn(Echo, HIGH);
  Fdistance= Fdistance/58;       
  return (int)Fdistance;
}
