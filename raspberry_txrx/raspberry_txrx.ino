#include <SoftwareSerial.h>

char c;
SoftwareSerial mySerial(2, 3); // RX, TX
int ledPin = 13;

void setup() {
//  Serial.begin(9600);
  mySerial.begin(9600);
  pinMode(ledPin, OUTPUT);
}

void loop() {
//  Serial.println(mySerial.read());
//  delay(500);
  if(mySerial.available() > 0) {
    digitalWrite(ledPin, HIGH);
    c = mySerial.read();
    if (c == 'A') {
      delay(1000);
      mySerial.print("OK");
    }
  }
}
