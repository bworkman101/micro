int m_back = 10;
int m_forw = 11;

void setup() {
  pinMode(m_back, OUTPUT);
  pinMode(m_forw, OUTPUT);
}

void loop() {
  digitalRead(m_back);
  analogWrite(m_forw, 255);
}
