#include <SoftwareSerial.h>

SoftwareSerial portBlue(2, 3); // rx, tx

String CAP = "\r\n";
String SLAVE_MODE = CAP + "+STWMOD=0" + CAP;
String INQUIRE = CAP + "+INQ=1" + CAP;
String RETURN_INQUIRE = CAP + "+RTINQ=";
String CONNECT = CAP + "+CONN=";
String REQUIRE_PIN = CAP + "+INPIN" + CAP;
String SEND_PIN = CAP + "+RTPIN=";
String DEVICE_NAME = CAP + "+STNA=";

String deviceName = "0";

void setup() {
  Serial.begin(9600);
  portBlue.begin(38400);
  
  pinMode(13, OUTPUT); // the led
  digitalWrite(13, LOW); // turn off led initially
  
}

void loop () {
  
  if (portBlue.available() > 0) {
    Serial.print("blue available");
    Serial.flush();
    digitalWrite(13, HIGH);
    int count = portBlue.available();
    for(int i = 0; i < count; i++) {
      Serial.write(portBlue.read());
      Serial.flush();
    }
    //digitalWrite(13, LOW);
  }
  
  if (Serial.available() > 0) {
    int cmd = Serial.read();
    if (cmd == 1) {
      Serial.print("setting master mode");
      Serial.flush();
      portBlue.print(SLAVE_MODE);
      portBlue.flush();
    }
    if (cmd == 2) {
      portBlue.print(INQUIRE);
      portBlue.flush();
      Serial.print("inquiring");
      Serial.flush();
    }
    
    if (cmd == 3) {
      if (deviceName == "0") {
        deviceName = DEVICE_NAME;
      } else {
        //Serial.print("setting name to (" + deviceName + ")");
        //Serial.flush();
        portBlue.print(deviceName + CAP);
        portBlue.flush();
        deviceName = "0";
      }
    } else if (deviceName != "0") {
      deviceName += char(cmd);
    }
  }
  
  /*digitalWrite(13, HIGH);
  delay(1000);
  digitalWrite(13, LOW);
  delay(1000);*/
}
