
#include <Servo.h>

int ENL=5; 
int RB=6;
int RF=7;
int ENR=11;
int LF=8;
int LB=9;
int Echo = A4;  
int Trig = A5;
int ServoPort = 3;
Servo servo;

void setup() {
  Serial.begin(115200);
  pinMode(LF, OUTPUT);
  pinMode(LB, OUTPUT);
  pinMode(RB, OUTPUT);
  pinMode(RF, OUTPUT);
  pinMode(ENL, OUTPUT);
  pinMode(ENR, OUTPUT);
  pinMode(Echo, INPUT);    
  pinMode(Trig, OUTPUT);
  
  digitalWrite(ENL,HIGH);  
  digitalWrite(ENR,HIGH);
  
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, HIGH);
  
  servo.attach(ServoPort);
  servo.write(90);
  delay(2000);
  servo.detach();
}

char rcvd[] = "    ";
short rcvdCnt = 0;
boolean ctrl = false;

void loop() {  
  if (Serial.available() > 0) {  
    char ch = Serial.read();
    if (ch == 'z') {
      if (!ctrl) {
        ctrl = true;
      }
      else if (ctrl && rcvdCnt == 4) {
        // everything received
        int val = charToNum(rcvd[1]) * 100 + charToNum(rcvd[2]) * 10 + charToNum(rcvd[3]);
        control(rcvd[0], val);
        ctrl = false;
        rcvdCnt = 0;
      }
      else {
        // if received is 0, this could be zz combo
        if (rcvdCnt > 0) {
          // noise, reset everything
          ctrl = false;
          rcvdCnt = 0;
        }
      }
    }
    else if (ctrl) {
      if (rcvdCnt == 4) {
        // noise
        ctrl = false;
        rcvdCnt = 0;
      }
      else {
        rcvd[rcvdCnt] = ch;
      }
      rcvdCnt++;
    }
  }
}

int distanceTest()   
{
  digitalWrite(Trig, LOW);   
  delayMicroseconds(2);
  digitalWrite(Trig, HIGH);  
  delayMicroseconds(20);
  digitalWrite(Trig, LOW);   
  float Fdistance = pulseIn(Echo, HIGH);  
  Fdistance= Fdistance/58;       
  return (int)Fdistance;
}

boolean shouldStop() {
  if (distanceTest() <= 20) {
    return true;
  } 
  else {
    return false;
  }
}

void control(int ctrlChar, int val) {
  switch (ctrlChar) {
    case 'l': {
      if (val <= 255)
      { // backward
        analogWrite(LF, 0);
        analogWrite(LB, 255 - val);
      }
      else
      { // forward
        analogWrite(LF, val - 255);
        analogWrite(LB, 0);
      }
      break;
    }
    case 'r': {
      if (val <= 255)
      { // backward
        analogWrite(RF, 0);
        analogWrite(RB, 255 - val);
      }
      else
      { // forward
        analogWrite(RF, val - 255);
        analogWrite(RB, 0);
      }
      break;
    }
    case 's': {
      doScan();
      break;
    }
  }
}

int charToNum(int ch) {
  switch(ch) {
    case '0': return 0;
    case '1': return 1;
    case '2': return 2;
    case '3': return 3;
    case '4': return 4;
    case '5': return 5;
    case '6': return 6;
    case '7': return 7;
    case '8': return 8;
    case '9': return 9;
    default: return 0;
  }
}

int angle = 90;
int dir = -1;
char scanMsg[20];

void doScan() {
  while(true) {
    if (!servo.attached()) {
      servo.attach(ServoPort);
    }
    servo.write(angle);
    
    if (Serial.available()) {
      char ch = Serial.read();
      if (ch == 's') {
        servo.write(90);
        delay(2000);
        servo.detach();
        return;
      }
    }
    
    int dist = distanceTest();
    sprintf(scanMsg, "[%d,%d]", dist, angle); 
    Serial.write(scanMsg);
    Serial.flush();
    angle = angle + dir;
    if (angle < 0) {
      dir = 1;
      angle = 1;
    }
    else if (angle > 180) {
      dir = -1;
      angle = 179;
    }
  }
}

