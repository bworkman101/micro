int led = 13;
int enable = 5;
int input1 = 6;
int input2 = 7;

// the setup routine runs once when you press reset:
void setup() {                
  pinMode(led, OUTPUT);
  pinMode(enable, OUTPUT);
  pinMode(input1, OUTPUT);
  pinMode(input2, OUTPUT);
  
  digitalWrite(enable, HIGH);
}

void loop() {
  digitalWrite(led, HIGH);
  delay(1000);
  digitalWrite(led, LOW);
  delay(1000);
}
