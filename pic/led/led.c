#include <p18f45j10.h>
#include <delays.h>

#pragma config WDTEN = OFF

void main (void)
{
  /* Make all bits on the Port B (LEDs) output bits.
   * If bit is cleared, then the bit is an output bit.
   */
  TRISB = 0;
  TRISC = 0;
  /* Reset the LEDs */
  PORTB = 0;
  PORTC = 0b00001110;

  //while (1) 
  //{
  //  PORTB = 0x1C;
  //	Delay1KTCYx(8); // guessing 8khz
  //  PORTB = 0;
  //	Delay1KTCYx(8);
  //}
}