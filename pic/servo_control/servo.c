#include <p18lf45j10.h>

#pragma config WDTEN = OFF

void _high_ISR(void);
void timerInterrupt(void);
void main (void);

int volatile led1On = 1;

#pragma code HIGH_INTERRUPT_VECTOR = 0x000008
	void _high_ISR(void) {
		_asm goto timerInterrupt _endasm
	}
#pragma code

#pragma interrupt timerInterrupt
	void timerInterrupt(void) {
		
		if (INTCONbits.TMR0IF == 1) {
			INTCONbits.TMR0IF = 0;
			if (led1On == 0) {
				PORTBbits.RB4 = 1;
				PORTDbits.RD1 = 1;
				led1On = 1;
			} else if (led1On == 1) {
				PORTBbits.RB4 = 0;
				PORTDbits.RD1 = 0;
				led1On = 0;
			}
			TMR0L = 12; 		// start timer at 30
		}

	}
#pragma code

void main ()
{
	T0CONbits.TMR0ON = 0;		//turn off timer 0

	TRISB = 0;
	TRISC = 0;
	TRISD = 0;	
	PORTB = 0;
	PORTC = 0;
	PORTD = 0;

	PORTDbits.RD1 = 1;
	PORTBbits.RB4 = 1;

	RCONbits.IPEN = 1;			//enable priority level interupts
	INTCONbits.GIE_GIEH = 1;	//enable high priority interupts, global interupt enabled
	INTCONbits.TMR0IE = 1;		//overflow interupt enabled
	INTCON2bits.TMR0IP = 1;		//high priority interupt
	T0CONbits.T08BIT = 1;
	T0CONbits.T0CS = 0;
	T0CONbits.PSA = 0;			// prescaler enabled
	T0CONbits.T0PS2 = 1;		// 1:256 prescaler
	T0CONbits.T0PS2 = 0;
	T0CONbits.T0PS2 = 0;

	T0CONbits.TMR0ON = 1;

	while(1) {}
}