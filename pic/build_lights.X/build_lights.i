#line 1 "build_lights.c"
#line 1 "build_lights.c"
#line 1 "/opt/microchip/mplabc18/v3.40/bin/../h/p18f4455.h"

#line 5 "/opt/microchip/mplabc18/v3.40/bin/../h/p18f4455.h"
 


#line 9 "/opt/microchip/mplabc18/v3.40/bin/../h/p18f4455.h"

extern volatile near unsigned char       SPPDATA;
extern volatile near unsigned char       SPPCFG;
extern volatile near union {
  struct {
    unsigned WS:4;
    unsigned CLK1EN:1;
    unsigned CSEN:1;
    unsigned CLKCFG:2;
  };
  struct {
    unsigned WS0:1;
    unsigned WS1:1;
    unsigned WS2:1;
    unsigned WS3:1;
    unsigned :2;
    unsigned CLKCFG0:1;
    unsigned CLKCFG1:1;
  };
} SPPCFGbits;
extern volatile near unsigned char       SPPEPS;
extern volatile near union {
  struct {
    unsigned ADDR:4;
    unsigned SPPBUSY:1;
    unsigned :1;
    unsigned WRSPP:1;
    unsigned RDSPP:1;
  };
  struct {
    unsigned ADDR0:1;
    unsigned ADDR1:1;
    unsigned ADDR2:1;
    unsigned ADDR3:1;
  };
} SPPEPSbits;
extern volatile near unsigned char       SPPCON;
extern volatile near struct {
  unsigned SPPEN:1;
  unsigned SPPOWN:1;
} SPPCONbits;
extern volatile near unsigned            UFRM;
extern volatile near unsigned char       UFRML;
extern volatile near union {
  struct {
    unsigned FRM:8;
  };
  struct {
    unsigned FRM0:1;
    unsigned FRM1:1;
    unsigned FRM2:1;
    unsigned FRM3:1;
    unsigned FRM4:1;
    unsigned FRM5:1;
    unsigned FRM6:1;
    unsigned FRM7:1;
  };
} UFRMLbits;
extern volatile near unsigned char       UFRMH;
extern volatile near union {
  struct {
    unsigned FRM:3;
  };
  struct {
    unsigned FRM8:1;
    unsigned FRM9:1;
    unsigned FRM10:1;
  };
} UFRMHbits;
extern volatile near unsigned char       UIR;
extern volatile near struct {
  unsigned URSTIF:1;
  unsigned UERRIF:1;
  unsigned ACTVIF:1;
  unsigned TRNIF:1;
  unsigned IDLEIF:1;
  unsigned STALLIF:1;
  unsigned SOFIF:1;
} UIRbits;
extern volatile near unsigned char       UIE;
extern volatile near struct {
  unsigned URSTIE:1;
  unsigned UERRIE:1;
  unsigned ACTVIE:1;
  unsigned TRNIE:1;
  unsigned IDLEIE:1;
  unsigned STALLIE:1;
  unsigned SOFIE:1;
} UIEbits;
extern volatile near unsigned char       UEIR;
extern volatile near struct {
  unsigned PIDEF:1;
  unsigned CRC5EF:1;
  unsigned CRC16EF:1;
  unsigned DFN8EF:1;
  unsigned BTOEF:1;
  unsigned :2;
  unsigned BTSEF:1;
} UEIRbits;
extern volatile near unsigned char       UEIE;
extern volatile near struct {
  unsigned PIDEE:1;
  unsigned CRC5EE:1;
  unsigned CRC16EE:1;
  unsigned DFN8EE:1;
  unsigned BTOEE:1;
  unsigned :2;
  unsigned BTSEE:1;
} UEIEbits;
extern volatile near unsigned char       USTAT;
extern volatile near union {
  struct {
    unsigned :1;
    unsigned PPBI:1;
    unsigned DIR:1;
    unsigned ENDP:4;
  };
  struct {
    unsigned :3;
    unsigned ENDP0:1;
    unsigned ENDP1:1;
    unsigned ENDP2:1;
    unsigned ENDP3:1;
  };
} USTATbits;
extern volatile near unsigned char       UCON;
extern volatile near struct {
  unsigned :1;
  unsigned SUSPND:1;
  unsigned RESUME:1;
  unsigned USBEN:1;
  unsigned PKTDIS:1;
  unsigned SE0:1;
  unsigned PPBRST:1;
} UCONbits;
extern volatile near unsigned char       UADDR;
extern volatile near union {
  struct {
    unsigned ADDR:7;
  };
  struct {
    unsigned ADDR0:1;
    unsigned ADDR1:1;
    unsigned ADDR2:1;
    unsigned ADDR3:1;
    unsigned ADDR4:1;
    unsigned ADDR5:1;
    unsigned ADDR6:1;
  };
} UADDRbits;
extern volatile near unsigned char       UCFG;
extern volatile near union {
  struct {
    unsigned PPB:2;
    unsigned FSEN:1;
    unsigned UTRDIS:1;
    unsigned UPUEN:1;
    unsigned :1;
    unsigned UOEMON:1;
    unsigned UTEYE:1;
  };
  struct {
    unsigned PPB0:1;
    unsigned PPB1:1;
  };
} UCFGbits;
extern volatile near unsigned char       UEP0;
extern volatile near struct {
  unsigned EPSTALL:1;
  unsigned EPINEN:1;
  unsigned EPOUTEN:1;
  unsigned EPCONDIS:1;
  unsigned EPHSHK:1;
} UEP0bits;
extern volatile near unsigned char       UEP1;
extern volatile near struct {
  unsigned EPSTALL:1;
  unsigned EPINEN:1;
  unsigned EPOUTEN:1;
  unsigned EPCONDIS:1;
  unsigned EPHSHK:1;
} UEP1bits;
extern volatile near unsigned char       UEP2;
extern volatile near struct {
  unsigned EPSTALL:1;
  unsigned EPINEN:1;
  unsigned EPOUTEN:1;
  unsigned EPCONDIS:1;
  unsigned EPHSHK:1;
} UEP2bits;
extern volatile near unsigned char       UEP3;
extern volatile near struct {
  unsigned EPSTALL:1;
  unsigned EPINEN:1;
  unsigned EPOUTEN:1;
  unsigned EPCONDIS:1;
  unsigned EPHSHK:1;
} UEP3bits;
extern volatile near unsigned char       UEP4;
extern volatile near struct {
  unsigned EPSTALL:1;
  unsigned EPINEN:1;
  unsigned EPOUTEN:1;
  unsigned EPCONDIS:1;
  unsigned EPHSHK:1;
} UEP4bits;
extern volatile near unsigned char       UEP5;
extern volatile near struct {
  unsigned EPSTALL:1;
  unsigned EPINEN:1;
  unsigned EPOUTEN:1;
  unsigned EPCONDIS:1;
  unsigned EPHSHK:1;
} UEP5bits;
extern volatile near unsigned char       UEP6;
extern volatile near struct {
  unsigned EPSTALL:1;
  unsigned EPINEN:1;
  unsigned EPOUTEN:1;
  unsigned EPCONDIS:1;
  unsigned EPHSHK:1;
} UEP6bits;
extern volatile near unsigned char       UEP7;
extern volatile near struct {
  unsigned EPSTALL:1;
  unsigned EPINEN:1;
  unsigned EPOUTEN:1;
  unsigned EPCONDIS:1;
  unsigned EPHSHK:1;
} UEP7bits;
extern volatile near unsigned char       UEP8;
extern volatile near struct {
  unsigned EPSTALL:1;
  unsigned EPINEN:1;
  unsigned EPOUTEN:1;
  unsigned EPCONDIS:1;
  unsigned EPHSHK:1;
} UEP8bits;
extern volatile near unsigned char       UEP9;
extern volatile near struct {
  unsigned EPSTALL:1;
  unsigned EPINEN:1;
  unsigned EPOUTEN:1;
  unsigned EPCONDIS:1;
  unsigned EPHSHK:1;
} UEP9bits;
extern volatile near unsigned char       UEP10;
extern volatile near struct {
  unsigned EPSTALL:1;
  unsigned EPINEN:1;
  unsigned EPOUTEN:1;
  unsigned EPCONDIS:1;
  unsigned EPHSHK:1;
} UEP10bits;
extern volatile near unsigned char       UEP11;
extern volatile near struct {
  unsigned EPSTALL:1;
  unsigned EPINEN:1;
  unsigned EPOUTEN:1;
  unsigned EPCONDIS:1;
  unsigned EPHSHK:1;
} UEP11bits;
extern volatile near unsigned char       UEP12;
extern volatile near struct {
  unsigned EPSTALL:1;
  unsigned EPINEN:1;
  unsigned EPOUTEN:1;
  unsigned EPCONDIS:1;
  unsigned EPHSHK:1;
} UEP12bits;
extern volatile near unsigned char       UEP13;
extern volatile near struct {
  unsigned EPSTALL:1;
  unsigned EPINEN:1;
  unsigned EPOUTEN:1;
  unsigned EPCONDIS:1;
  unsigned EPHSHK:1;
} UEP13bits;
extern volatile near unsigned char       UEP14;
extern volatile near struct {
  unsigned EPSTALL:1;
  unsigned EPINEN:1;
  unsigned EPOUTEN:1;
  unsigned EPCONDIS:1;
  unsigned EPHSHK:1;
} UEP14bits;
extern volatile near unsigned char       UEP15;
extern volatile near struct {
  unsigned EPSTALL:1;
  unsigned EPINEN:1;
  unsigned EPOUTEN:1;
  unsigned EPCONDIS:1;
  unsigned EPHSHK:1;
} UEP15bits;
extern volatile near unsigned char       PORTA;
extern volatile near union {
  struct {
    unsigned RA0:1;
    unsigned RA1:1;
    unsigned RA2:1;
    unsigned RA3:1;
    unsigned RA4:1;
    unsigned RA5:1;
    unsigned RA6:1;
  };
  struct {
    unsigned AN0:1;
    unsigned AN1:1;
    unsigned AN2:1;
    unsigned AN3:1;
    unsigned T0CKI:1;
    unsigned AN4:1;
    unsigned OSC2:1;
  };
  struct {
    unsigned :2;
    unsigned VREFM:1;
    unsigned VREFP:1;
    unsigned :1;
    unsigned LVDIN:1;
  };
  struct {
    unsigned :5;
    unsigned HLVDIN:1;
  };
} PORTAbits;
extern volatile near unsigned char       PORTB;
extern volatile near union {
  struct {
    unsigned RB0:1;
    unsigned RB1:1;
    unsigned RB2:1;
    unsigned RB3:1;
    unsigned RB4:1;
    unsigned RB5:1;
    unsigned RB6:1;
    unsigned RB7:1;
  };
  struct {
    unsigned INT0:1;
    unsigned INT1:1;
    unsigned INT2:1;
    unsigned :2;
    unsigned PGM:1;
    unsigned PGC:1;
    unsigned PGD:1;
  };
} PORTBbits;
extern volatile near unsigned char       PORTC;
extern volatile near union {
  struct {
    unsigned RC0:1;
    unsigned RC1:1;
    unsigned RC2:1;
    unsigned :1;
    unsigned RC4:1;
    unsigned RC5:1;
    unsigned RC6:1;
    unsigned RC7:1;
  };
  struct {
    unsigned T1OSO:1;
    unsigned T1OSI:1;
    unsigned CCP1:1;
    unsigned :3;
    unsigned TX:1;
    unsigned RX:1;
  };
  struct {
    unsigned T13CKI:1;
    unsigned :1;
    unsigned P1A:1;
    unsigned :3;
    unsigned CK:1;
    unsigned DT:1;
  };
} PORTCbits;
extern volatile near unsigned char       PORTD;
extern volatile near union {
  struct {
    unsigned RD0:1;
    unsigned RD1:1;
    unsigned RD2:1;
    unsigned RD3:1;
    unsigned RD4:1;
    unsigned RD5:1;
    unsigned RD6:1;
    unsigned RD7:1;
  };
  struct {
    unsigned SPP0:1;
    unsigned SPP1:1;
    unsigned SPP2:1;
    unsigned SPP3:1;
    unsigned SPP4:1;
    unsigned SPP5:1;
    unsigned SPP6:1;
    unsigned SPP7:1;
  };
} PORTDbits;
extern volatile near unsigned char       PORTE;
extern volatile near union {
  struct {
    unsigned RE0:1;
    unsigned RE1:1;
    unsigned RE2:1;
    unsigned RE3:1;
    unsigned :3;
    unsigned RDPU:1;
  };
  struct {
    unsigned CK1SPP:1;
    unsigned CK2SPP:1;
    unsigned OESPP:1;
  };
} PORTEbits;
extern volatile near unsigned char       LATA;
extern volatile near struct {
  unsigned LATA0:1;
  unsigned LATA1:1;
  unsigned LATA2:1;
  unsigned LATA3:1;
  unsigned LATA4:1;
  unsigned LATA5:1;
  unsigned LATA6:1;
} LATAbits;
extern volatile near unsigned char       LATB;
extern volatile near struct {
  unsigned LATB0:1;
  unsigned LATB1:1;
  unsigned LATB2:1;
  unsigned LATB3:1;
  unsigned LATB4:1;
  unsigned LATB5:1;
  unsigned LATB6:1;
  unsigned LATB7:1;
} LATBbits;
extern volatile near unsigned char       LATC;
extern volatile near struct {
  unsigned LATC0:1;
  unsigned LATC1:1;
  unsigned LATC2:1;
  unsigned :3;
  unsigned LATC6:1;
  unsigned LATC7:1;
} LATCbits;
extern volatile near unsigned char       LATD;
extern volatile near struct {
  unsigned LATD0:1;
  unsigned LATD1:1;
  unsigned LATD2:1;
  unsigned LATD3:1;
  unsigned LATD4:1;
  unsigned LATD5:1;
  unsigned LATD6:1;
  unsigned LATD7:1;
} LATDbits;
extern volatile near unsigned char       LATE;
extern volatile near struct {
  unsigned LATE0:1;
  unsigned LATE1:1;
  unsigned LATE2:1;
} LATEbits;
extern volatile near unsigned char       DDRA;
extern volatile near union {
  struct {
    unsigned TRISA0:1;
    unsigned TRISA1:1;
    unsigned TRISA2:1;
    unsigned TRISA3:1;
    unsigned TRISA4:1;
    unsigned TRISA5:1;
    unsigned TRISA6:1;
  };
  struct {
    unsigned RA0:1;
    unsigned RA1:1;
    unsigned RA2:1;
    unsigned RA3:1;
    unsigned RA4:1;
    unsigned RA5:1;
    unsigned RA6:1;
  };
} DDRAbits;
extern volatile near unsigned char       TRISA;
extern volatile near union {
  struct {
    unsigned TRISA0:1;
    unsigned TRISA1:1;
    unsigned TRISA2:1;
    unsigned TRISA3:1;
    unsigned TRISA4:1;
    unsigned TRISA5:1;
    unsigned TRISA6:1;
  };
  struct {
    unsigned RA0:1;
    unsigned RA1:1;
    unsigned RA2:1;
    unsigned RA3:1;
    unsigned RA4:1;
    unsigned RA5:1;
    unsigned RA6:1;
  };
} TRISAbits;
extern volatile near unsigned char       DDRB;
extern volatile near union {
  struct {
    unsigned TRISB0:1;
    unsigned TRISB1:1;
    unsigned TRISB2:1;
    unsigned TRISB3:1;
    unsigned TRISB4:1;
    unsigned TRISB5:1;
    unsigned TRISB6:1;
    unsigned TRISB7:1;
  };
  struct {
    unsigned RB0:1;
    unsigned RB1:1;
    unsigned RB2:1;
    unsigned RB3:1;
    unsigned RB4:1;
    unsigned RB5:1;
    unsigned RB6:1;
    unsigned RB7:1;
  };
} DDRBbits;
extern volatile near unsigned char       TRISB;
extern volatile near union {
  struct {
    unsigned TRISB0:1;
    unsigned TRISB1:1;
    unsigned TRISB2:1;
    unsigned TRISB3:1;
    unsigned TRISB4:1;
    unsigned TRISB5:1;
    unsigned TRISB6:1;
    unsigned TRISB7:1;
  };
  struct {
    unsigned RB0:1;
    unsigned RB1:1;
    unsigned RB2:1;
    unsigned RB3:1;
    unsigned RB4:1;
    unsigned RB5:1;
    unsigned RB6:1;
    unsigned RB7:1;
  };
} TRISBbits;
extern volatile near unsigned char       DDRC;
extern volatile near union {
  struct {
    unsigned TRISC0:1;
    unsigned TRISC1:1;
    unsigned TRISC2:1;
    unsigned :3;
    unsigned TRISC6:1;
    unsigned TRISC7:1;
  };
  struct {
    unsigned RC0:1;
    unsigned RC1:1;
    unsigned RC2:1;
    unsigned :3;
    unsigned RC6:1;
    unsigned RC7:1;
  };
} DDRCbits;
extern volatile near unsigned char       TRISC;
extern volatile near union {
  struct {
    unsigned TRISC0:1;
    unsigned TRISC1:1;
    unsigned TRISC2:1;
    unsigned :3;
    unsigned TRISC6:1;
    unsigned TRISC7:1;
  };
  struct {
    unsigned RC0:1;
    unsigned RC1:1;
    unsigned RC2:1;
    unsigned :3;
    unsigned RC6:1;
    unsigned RC7:1;
  };
} TRISCbits;
extern volatile near unsigned char       DDRD;
extern volatile near union {
  struct {
    unsigned TRISD0:1;
    unsigned TRISD1:1;
    unsigned TRISD2:1;
    unsigned TRISD3:1;
    unsigned TRISD4:1;
    unsigned TRISD5:1;
    unsigned TRISD6:1;
    unsigned TRISD7:1;
  };
  struct {
    unsigned RD0:1;
    unsigned RD1:1;
    unsigned RD2:1;
    unsigned RD3:1;
    unsigned RD4:1;
    unsigned RD5:1;
    unsigned RD6:1;
    unsigned RD7:1;
  };
} DDRDbits;
extern volatile near unsigned char       TRISD;
extern volatile near union {
  struct {
    unsigned TRISD0:1;
    unsigned TRISD1:1;
    unsigned TRISD2:1;
    unsigned TRISD3:1;
    unsigned TRISD4:1;
    unsigned TRISD5:1;
    unsigned TRISD6:1;
    unsigned TRISD7:1;
  };
  struct {
    unsigned RD0:1;
    unsigned RD1:1;
    unsigned RD2:1;
    unsigned RD3:1;
    unsigned RD4:1;
    unsigned RD5:1;
    unsigned RD6:1;
    unsigned RD7:1;
  };
} TRISDbits;
extern volatile near unsigned char       DDRE;
extern volatile near union {
  struct {
    unsigned TRISE0:1;
    unsigned TRISE1:1;
    unsigned TRISE2:1;
  };
  struct {
    unsigned RE0:1;
    unsigned RE1:1;
    unsigned RE2:1;
  };
} DDREbits;
extern volatile near unsigned char       TRISE;
extern volatile near union {
  struct {
    unsigned TRISE0:1;
    unsigned TRISE1:1;
    unsigned TRISE2:1;
  };
  struct {
    unsigned RE0:1;
    unsigned RE1:1;
    unsigned RE2:1;
  };
} TRISEbits;
extern volatile near unsigned char       OSCTUNE;
extern volatile near union {
  struct {
    unsigned TUN:5;
    unsigned :2;
    unsigned INTSRC:1;
  };
  struct {
    unsigned TUN0:1;
    unsigned TUN1:1;
    unsigned TUN2:1;
    unsigned TUN3:1;
    unsigned TUN4:1;
  };
} OSCTUNEbits;
extern volatile near unsigned char       PIE1;
extern volatile near struct {
  unsigned TMR1IE:1;
  unsigned TMR2IE:1;
  unsigned CCP1IE:1;
  unsigned SSPIE:1;
  unsigned TXIE:1;
  unsigned RCIE:1;
  unsigned ADIE:1;
  unsigned SPPIE:1;
} PIE1bits;
extern volatile near unsigned char       PIR1;
extern volatile near struct {
  unsigned TMR1IF:1;
  unsigned TMR2IF:1;
  unsigned CCP1IF:1;
  unsigned SSPIF:1;
  unsigned TXIF:1;
  unsigned RCIF:1;
  unsigned ADIF:1;
  unsigned SPPIF:1;
} PIR1bits;
extern volatile near unsigned char       IPR1;
extern volatile near struct {
  unsigned TMR1IP:1;
  unsigned TMR2IP:1;
  unsigned CCP1IP:1;
  unsigned SSPIP:1;
  unsigned TXIP:1;
  unsigned RCIP:1;
  unsigned ADIP:1;
  unsigned SPPIP:1;
} IPR1bits;
extern volatile near unsigned char       PIE2;
extern volatile near union {
  struct {
    unsigned CCP2IE:1;
    unsigned TMR3IE:1;
    unsigned HLVDIE:1;
    unsigned BCLIE:1;
    unsigned EEIE:1;
    unsigned USBIE:1;
    unsigned CMIE:1;
    unsigned OSCFIE:1;
  };
  struct {
    unsigned :2;
    unsigned LVDIE:1;
  };
} PIE2bits;
extern volatile near unsigned char       PIR2;
extern volatile near union {
  struct {
    unsigned CCP2IF:1;
    unsigned TMR3IF:1;
    unsigned HLVDIF:1;
    unsigned BCLIF:1;
    unsigned EEIF:1;
    unsigned USBIF:1;
    unsigned CMIF:1;
    unsigned OSCFIF:1;
  };
  struct {
    unsigned :2;
    unsigned LVDIF:1;
  };
} PIR2bits;
extern volatile near unsigned char       IPR2;
extern volatile near union {
  struct {
    unsigned CCP2IP:1;
    unsigned TMR3IP:1;
    unsigned HLVDIP:1;
    unsigned BCLIP:1;
    unsigned EEIP:1;
    unsigned USBIP:1;
    unsigned CMIP:1;
    unsigned OSCFIP:1;
  };
  struct {
    unsigned :2;
    unsigned LVDIP:1;
  };
} IPR2bits;
extern volatile near unsigned char       EECON1;
extern volatile near struct {
  unsigned RD:1;
  unsigned WR:1;
  unsigned WREN:1;
  unsigned WRERR:1;
  unsigned FREE:1;
  unsigned :1;
  unsigned CFGS:1;
  unsigned EEPGD:1;
} EECON1bits;
extern volatile near unsigned char       EECON2;
extern volatile near unsigned char       EEDATA;
extern volatile near unsigned char       EEADR;
extern volatile near unsigned char       RCSTA;
extern volatile near union {
  struct {
    unsigned RX9D:1;
    unsigned OERR:1;
    unsigned FERR:1;
    unsigned ADDEN:1;
    unsigned CREN:1;
    unsigned SREN:1;
    unsigned RX9:1;
    unsigned SPEN:1;
  };
  struct {
    unsigned :3;
    unsigned ADEN:1;
  };
} RCSTAbits;
extern volatile near unsigned char       TXSTA;
extern volatile near struct {
  unsigned TX9D:1;
  unsigned TRMT:1;
  unsigned BRGH:1;
  unsigned SENDB:1;
  unsigned SYNC:1;
  unsigned TXEN:1;
  unsigned TX9:1;
  unsigned CSRC:1;
} TXSTAbits;
extern volatile near unsigned char       TXREG;
extern volatile near unsigned char       RCREG;
extern volatile near unsigned char       SPBRG;
extern volatile near unsigned char       SPBRGH;
extern volatile near unsigned char       T3CON;
extern volatile near union {
  struct {
    unsigned TMR3ON:1;
    unsigned TMR3CS:1;
    unsigned NOT_T3SYNC:1;
    unsigned T3CCP1:1;
    unsigned T3CKPS:2;
    unsigned T3CCP2:1;
    unsigned RD16:1;
  };
  struct {
    unsigned :2;
    unsigned T3SYNC:1;
    unsigned :1;
    unsigned T3CKPS0:1;
    unsigned T3CKPS1:1;
  };
  struct {
    unsigned :2;
    unsigned T3NSYNC:1;
  };
} T3CONbits;
extern volatile near unsigned char       TMR3L;
extern volatile near unsigned char       TMR3H;
extern volatile near unsigned char       CMCON;
extern volatile near union {
  struct {
    unsigned CM:3;
    unsigned CIS:1;
    unsigned C1INV:1;
    unsigned C2INV:1;
    unsigned C1OUT:1;
    unsigned C2OUT:1;
  };
  struct {
    unsigned CM0:1;
    unsigned CM1:1;
    unsigned CM2:1;
  };
} CMCONbits;
extern volatile near unsigned char       CVRCON;
extern volatile near union {
  struct {
    unsigned CVR:4;
    unsigned CVRSS:1;
    unsigned CVRR:1;
    unsigned CVROE:1;
    unsigned CVREN:1;
  };
  struct {
    unsigned CVR0:1;
    unsigned CVR1:1;
    unsigned CVR2:1;
    unsigned CVR3:1;
    unsigned CVREF:1;
  };
} CVRCONbits;
extern volatile near unsigned char       CCP1AS;
extern volatile near union {
  struct {
    unsigned PSSBD:2;
    unsigned PSSAC:2;
    unsigned ECCPAS:3;
    unsigned ECCPASE:1;
  };
  struct {
    unsigned PSSBD0:1;
    unsigned PSSBD1:1;
    unsigned PSSAC0:1;
    unsigned PSSAC1:1;
    unsigned ECCPAS0:1;
    unsigned ECCPAS1:1;
    unsigned ECCPAS2:1;
  };
} CCP1ASbits;
extern volatile near unsigned char       ECCP1AS;
extern volatile near union {
  struct {
    unsigned PSSBD:2;
    unsigned PSSAC:2;
    unsigned ECCPAS:3;
    unsigned ECCPASE:1;
  };
  struct {
    unsigned PSSBD0:1;
    unsigned PSSBD1:1;
    unsigned PSSAC0:1;
    unsigned PSSAC1:1;
    unsigned ECCPAS0:1;
    unsigned ECCPAS1:1;
    unsigned ECCPAS2:1;
  };
} ECCP1ASbits;
extern volatile near unsigned char       CCP1DEL;
extern volatile near union {
  struct {
    unsigned PDC:7;
    unsigned PRSEN:1;
  };
  struct {
    unsigned PDC0:1;
    unsigned PDC1:1;
    unsigned PDC2:1;
    unsigned PDC3:1;
    unsigned PDC4:1;
    unsigned PDC5:1;
    unsigned PDC6:1;
  };
} CCP1DELbits;
extern volatile near unsigned char       ECCP1DEL;
extern volatile near union {
  struct {
    unsigned PDC:7;
    unsigned PRSEN:1;
  };
  struct {
    unsigned PDC0:1;
    unsigned PDC1:1;
    unsigned PDC2:1;
    unsigned PDC3:1;
    unsigned PDC4:1;
    unsigned PDC5:1;
    unsigned PDC6:1;
  };
} ECCP1DELbits;
extern volatile near unsigned char       BAUDCON;
extern volatile near union {
  struct {
    unsigned ABDEN:1;
    unsigned WUE:1;
    unsigned :1;
    unsigned BRG16:1;
    unsigned TXCKP:1;
    unsigned RXDTP:1;
    unsigned RCIDL:1;
    unsigned ABDOVF:1;
  };
  struct {
    unsigned :4;
    unsigned SCKP:1;
    unsigned :1;
    unsigned RCMT:1;
  };
} BAUDCONbits;
extern volatile near unsigned char       BAUDCTL;
extern volatile near union {
  struct {
    unsigned ABDEN:1;
    unsigned WUE:1;
    unsigned :1;
    unsigned BRG16:1;
    unsigned TXCKP:1;
    unsigned RXDTP:1;
    unsigned RCIDL:1;
    unsigned ABDOVF:1;
  };
  struct {
    unsigned :4;
    unsigned SCKP:1;
    unsigned :1;
    unsigned RCMT:1;
  };
} BAUDCTLbits;
extern volatile near unsigned char       CCP2CON;
extern volatile near union {
  struct {
    unsigned CCP2M:4;
    unsigned DC2B:2;
  };
  struct {
    unsigned CCP2M0:1;
    unsigned CCP2M1:1;
    unsigned CCP2M2:1;
    unsigned CCP2M3:1;
    unsigned DC2B0:1;
    unsigned DC2B1:1;
  };
} CCP2CONbits;
extern volatile near unsigned            CCPR2;
extern volatile near unsigned char       CCPR2L;
extern volatile near unsigned char       CCPR2H;
extern volatile near unsigned char       CCP1CON;
extern volatile near union {
  struct {
    unsigned CCP1M:4;
    unsigned DC1B:2;
    unsigned P1M:2;
  };
  struct {
    unsigned CCP1M0:1;
    unsigned CCP1M1:1;
    unsigned CCP1M2:1;
    unsigned CCP1M3:1;
    unsigned DC1B0:1;
    unsigned DC1B1:1;
    unsigned P1M0:1;
    unsigned P1M1:1;
  };
} CCP1CONbits;
extern volatile near unsigned char       ECCP1CON;
extern volatile near union {
  struct {
    unsigned CCP1M:4;
    unsigned DC1B:2;
    unsigned P1M:2;
  };
  struct {
    unsigned CCP1M0:1;
    unsigned CCP1M1:1;
    unsigned CCP1M2:1;
    unsigned CCP1M3:1;
    unsigned DC1B0:1;
    unsigned DC1B1:1;
    unsigned P1M0:1;
    unsigned P1M1:1;
  };
} ECCP1CONbits;
extern volatile near unsigned            CCPR1;
extern volatile near unsigned char       CCPR1L;
extern volatile near unsigned char       CCPR1H;
extern volatile near unsigned char       ADCON2;
extern volatile near union {
  struct {
    unsigned ADCS:3;
    unsigned ACQT:3;
    unsigned :1;
    unsigned ADFM:1;
  };
  struct {
    unsigned ADCS0:1;
    unsigned ADCS1:1;
    unsigned ADCS2:1;
    unsigned ACQT0:1;
    unsigned ACQT1:1;
    unsigned ACQT2:1;
  };
} ADCON2bits;
extern volatile near unsigned char       ADCON1;
extern volatile near union {
  struct {
    unsigned PCFG:4;
    unsigned VCFG:2;
  };
  struct {
    unsigned PCFG0:1;
    unsigned PCFG1:1;
    unsigned PCFG2:1;
    unsigned PCFG3:1;
    unsigned VCFG0:1;
    unsigned VCFG1:1;
  };
} ADCON1bits;
extern volatile near unsigned char       ADCON0;
extern volatile near union {
  struct {
    unsigned ADON:1;
    unsigned GO_NOT_DONE:1;
    unsigned CHS:4;
  };
  struct {
    unsigned :1;
    unsigned GO_DONE:1;
    unsigned CHS0:1;
    unsigned CHS1:1;
    unsigned CHS2:1;
    unsigned CHS3:1;
  };
  struct {
    unsigned :1;
    unsigned DONE:1;
  };
  struct {
    unsigned :1;
    unsigned GO:1;
  };
  struct {
    unsigned :1;
    unsigned NOT_DONE:1;
  };
} ADCON0bits;
extern volatile near unsigned            ADRES;
extern volatile near unsigned char       ADRESL;
extern volatile near unsigned char       ADRESH;
extern volatile near unsigned char       SSPCON2;
extern volatile near struct {
  unsigned SEN:1;
  unsigned RSEN:1;
  unsigned PEN:1;
  unsigned RCEN:1;
  unsigned ACKEN:1;
  unsigned ACKDT:1;
  unsigned ACKSTAT:1;
  unsigned GCEN:1;
} SSPCON2bits;
extern volatile near unsigned char       SSPCON1;
extern volatile near union {
  struct {
    unsigned SSPM:4;
    unsigned CKP:1;
    unsigned SSPEN:1;
    unsigned SSPOV:1;
    unsigned WCOL:1;
  };
  struct {
    unsigned SSPM0:1;
    unsigned SSPM1:1;
    unsigned SSPM2:1;
    unsigned SSPM3:1;
  };
} SSPCON1bits;
extern volatile near unsigned char       SSPSTAT;
extern volatile near union {
  struct {
    unsigned BF:1;
    unsigned UA:1;
    unsigned R_NOT_W:1;
    unsigned S:1;
    unsigned P:1;
    unsigned D_NOT_A:1;
    unsigned CKE:1;
    unsigned SMP:1;
  };
  struct {
    unsigned :2;
    unsigned R_W:1;
    unsigned :2;
    unsigned D_A:1;
  };
  struct {
    unsigned :2;
    unsigned I2C_READ:1;
    unsigned I2C_START:1;
    unsigned I2C_STOP:1;
    unsigned I2C_DAT:1;
  };
  struct {
    unsigned :2;
    unsigned NOT_W:1;
    unsigned :2;
    unsigned NOT_A:1;
  };
  struct {
    unsigned :2;
    unsigned NOT_WRITE:1;
    unsigned :2;
    unsigned NOT_ADDRESS:1;
  };
  struct {
    unsigned :2;
    unsigned READ_WRITE:1;
    unsigned :2;
    unsigned DATA_ADDRESS:1;
  };
  struct {
    unsigned :2;
    unsigned R:1;
    unsigned :2;
    unsigned D:1;
  };
} SSPSTATbits;
extern volatile near unsigned char       SSPADD;
extern volatile near unsigned char       SSPBUF;
extern volatile near unsigned char       T2CON;
extern volatile near union {
  struct {
    unsigned T2CKPS:2;
    unsigned TMR2ON:1;
    unsigned TOUTPS:4;
  };
  struct {
    unsigned T2CKPS0:1;
    unsigned T2CKPS1:1;
    unsigned :1;
    unsigned T2OUTPS0:1;
    unsigned T2OUTPS1:1;
    unsigned T2OUTPS2:1;
    unsigned T2OUTPS3:1;
  };
  struct {
    unsigned :3;
    unsigned TOUTPS0:1;
    unsigned TOUTPS1:1;
    unsigned TOUTPS2:1;
    unsigned TOUTPS3:1;
  };
} T2CONbits;
extern volatile near unsigned char       PR2;
extern volatile near unsigned char       TMR2;
extern volatile near unsigned char       T1CON;
extern volatile near union {
  struct {
    unsigned TMR1ON:1;
    unsigned TMR1CS:1;
    unsigned NOT_T1SYNC:1;
    unsigned T1OSCEN:1;
    unsigned T1CKPS:2;
    unsigned T1RUN:1;
    unsigned RD16:1;
  };
  struct {
    unsigned :2;
    unsigned T1SYNC:1;
    unsigned :1;
    unsigned T1CKPS0:1;
    unsigned T1CKPS1:1;
  };
} T1CONbits;
extern volatile near unsigned char       TMR1L;
extern volatile near unsigned char       TMR1H;
extern volatile near unsigned char       RCON;
extern volatile near union {
  struct {
    unsigned NOT_BOR:1;
    unsigned NOT_POR:1;
    unsigned NOT_PD:1;
    unsigned NOT_TO:1;
    unsigned NOT_RI:1;
    unsigned :1;
    unsigned SBOREN:1;
    unsigned IPEN:1;
  };
  struct {
    unsigned BOR:1;
    unsigned POR:1;
    unsigned PD:1;
    unsigned TO:1;
    unsigned RI:1;
    unsigned :2;
    unsigned NOT_IPEN:1;
  };
} RCONbits;
extern volatile near unsigned char       WDTCON;
extern volatile near union {
  struct {
    unsigned SWDTEN:1;
  };
  struct {
    unsigned SWDTE:1;
  };
} WDTCONbits;
extern volatile near unsigned char       HLVDCON;
extern volatile near union {
  struct {
    unsigned HLVDL:4;
    unsigned HLVDEN:1;
    unsigned IRVST:1;
    unsigned :1;
    unsigned VDIRMAG:1;
  };
  struct {
    unsigned HLVDL0:1;
    unsigned HLVDL1:1;
    unsigned HLVDL2:1;
    unsigned HLVDL3:1;
  };
  struct {
    unsigned LVDL0:1;
    unsigned LVDL1:1;
    unsigned LVDL2:1;
    unsigned LVDL3:1;
    unsigned LVDEN:1;
    unsigned IVRST:1;
  };
  struct {
    unsigned LVV0:1;
    unsigned LVV1:1;
    unsigned LVV2:1;
    unsigned LVV3:1;
    unsigned :1;
    unsigned BGST:1;
  };
} HLVDCONbits;
extern volatile near unsigned char       LVDCON;
extern volatile near union {
  struct {
    unsigned HLVDL:4;
    unsigned HLVDEN:1;
    unsigned IRVST:1;
    unsigned :1;
    unsigned VDIRMAG:1;
  };
  struct {
    unsigned HLVDL0:1;
    unsigned HLVDL1:1;
    unsigned HLVDL2:1;
    unsigned HLVDL3:1;
  };
  struct {
    unsigned LVDL0:1;
    unsigned LVDL1:1;
    unsigned LVDL2:1;
    unsigned LVDL3:1;
    unsigned LVDEN:1;
    unsigned IVRST:1;
  };
  struct {
    unsigned LVV0:1;
    unsigned LVV1:1;
    unsigned LVV2:1;
    unsigned LVV3:1;
    unsigned :1;
    unsigned BGST:1;
  };
} LVDCONbits;
extern volatile near unsigned char       OSCCON;
extern volatile near union {
  struct {
    unsigned SCS:2;
    unsigned IOFS:1;
    unsigned OSTS:1;
    unsigned IRCF:3;
    unsigned IDLEN:1;
  };
  struct {
    unsigned SCS0:1;
    unsigned SCS1:1;
    unsigned FLTS:1;
    unsigned :1;
    unsigned IRCF0:1;
    unsigned IRCF1:1;
    unsigned IRCF2:1;
  };
} OSCCONbits;
extern volatile near unsigned char       T0CON;
extern volatile near union {
  struct {
    unsigned T0PS:3;
    unsigned PSA:1;
    unsigned T0SE:1;
    unsigned T0CS:1;
    unsigned T08BIT:1;
    unsigned TMR0ON:1;
  };
  struct {
    unsigned T0PS0:1;
    unsigned T0PS1:1;
    unsigned T0PS2:1;
  };
} T0CONbits;
extern volatile near unsigned char       TMR0L;
extern volatile near unsigned char       TMR0H;
extern          near unsigned char       STATUS;
extern          near struct {
  unsigned C:1;
  unsigned DC:1;
  unsigned Z:1;
  unsigned OV:1;
  unsigned N:1;
} STATUSbits;
extern          near unsigned            FSR2;
extern          near unsigned char       FSR2L;
extern          near unsigned char       FSR2H;
extern volatile near unsigned char       PLUSW2;
extern volatile near unsigned char       PREINC2;
extern volatile near unsigned char       POSTDEC2;
extern volatile near unsigned char       POSTINC2;
extern          near unsigned char       INDF2;
extern          near unsigned char       BSR;
extern          near unsigned            FSR1;
extern          near unsigned char       FSR1L;
extern          near unsigned char       FSR1H;
extern volatile near unsigned char       PLUSW1;
extern volatile near unsigned char       PREINC1;
extern volatile near unsigned char       POSTDEC1;
extern volatile near unsigned char       POSTINC1;
extern          near unsigned char       INDF1;
extern          near unsigned char       WREG;
extern          near unsigned            FSR0;
extern          near unsigned char       FSR0L;
extern          near unsigned char       FSR0H;
extern volatile near unsigned char       PLUSW0;
extern volatile near unsigned char       PREINC0;
extern volatile near unsigned char       POSTDEC0;
extern volatile near unsigned char       POSTINC0;
extern          near unsigned char       INDF0;
extern volatile near unsigned char       INTCON3;
extern volatile near union {
  struct {
    unsigned INT1IF:1;
    unsigned INT2IF:1;
    unsigned :1;
    unsigned INT1IE:1;
    unsigned INT2IE:1;
    unsigned :1;
    unsigned INT1IP:1;
    unsigned INT2IP:1;
  };
  struct {
    unsigned INT1F:1;
    unsigned INT2F:1;
    unsigned :1;
    unsigned INT1E:1;
    unsigned INT2E:1;
    unsigned :1;
    unsigned INT1P:1;
    unsigned INT2P:1;
  };
} INTCON3bits;
extern volatile near unsigned char       INTCON2;
extern volatile near union {
  struct {
    unsigned RBIP:1;
    unsigned :1;
    unsigned TMR0IP:1;
    unsigned :1;
    unsigned INTEDG2:1;
    unsigned INTEDG1:1;
    unsigned INTEDG0:1;
    unsigned NOT_RBPU:1;
  };
  struct {
    unsigned :2;
    unsigned T0IP:1;
    unsigned :4;
    unsigned RBPU:1;
  };
} INTCON2bits;
extern volatile near unsigned char       INTCON;
extern volatile near union {
  struct {
    unsigned RBIF:1;
    unsigned INT0IF:1;
    unsigned TMR0IF:1;
    unsigned RBIE:1;
    unsigned INT0IE:1;
    unsigned TMR0IE:1;
    unsigned PEIE_GIEL:1;
    unsigned GIE_GIEH:1;
  };
  struct {
    unsigned :1;
    unsigned INT0F:1;
    unsigned T0IF:1;
    unsigned :1;
    unsigned INT0E:1;
    unsigned T0IE:1;
    unsigned PEIE:1;
    unsigned GIE:1;
  };
  struct {
    unsigned :6;
    unsigned GIEL:1;
    unsigned GIEH:1;
  };
} INTCONbits;
extern          near unsigned            PROD;
extern          near unsigned char       PRODL;
extern          near unsigned char       PRODH;
extern volatile near unsigned char       TABLAT;
extern volatile near unsigned short long TBLPTR;
extern volatile near unsigned char       TBLPTRL;
extern volatile near unsigned char       TBLPTRH;
extern volatile near unsigned char       TBLPTRU;
extern volatile near unsigned short long PC;
extern volatile near unsigned char       PCL;
extern volatile near unsigned char       PCLATH;
extern volatile near unsigned char       PCLATU;
extern volatile near unsigned char       STKPTR;
extern volatile near union {
  struct {
    unsigned STKPTR:5;
    unsigned :1;
    unsigned STKUNF:1;
    unsigned STKFUL:1;
  };
  struct {
    unsigned STKPTR0:1;
    unsigned STKPTR1:1;
    unsigned STKPTR2:1;
    unsigned STKPTR3:1;
    unsigned STKPTR4:1;
  };
  struct {
    unsigned :7;
    unsigned STKOVF:1;
  };
} STKPTRbits;
extern          near unsigned short long TOS;
extern          near unsigned char       TOSL;
extern          near unsigned char       TOSH;
extern          near unsigned char       TOSU;



#line 1501 "/opt/microchip/mplabc18/v3.40/bin/../h/p18f4455.h"
 
#line 1503 "/opt/microchip/mplabc18/v3.40/bin/../h/p18f4455.h"
#line 1504 "/opt/microchip/mplabc18/v3.40/bin/../h/p18f4455.h"


#line 1507 "/opt/microchip/mplabc18/v3.40/bin/../h/p18f4455.h"
 
#line 1509 "/opt/microchip/mplabc18/v3.40/bin/../h/p18f4455.h"
#line 1510 "/opt/microchip/mplabc18/v3.40/bin/../h/p18f4455.h"
#line 1511 "/opt/microchip/mplabc18/v3.40/bin/../h/p18f4455.h"
#line 1512 "/opt/microchip/mplabc18/v3.40/bin/../h/p18f4455.h"

#line 1514 "/opt/microchip/mplabc18/v3.40/bin/../h/p18f4455.h"
#line 1515 "/opt/microchip/mplabc18/v3.40/bin/../h/p18f4455.h"
#line 1516 "/opt/microchip/mplabc18/v3.40/bin/../h/p18f4455.h"
#line 1517 "/opt/microchip/mplabc18/v3.40/bin/../h/p18f4455.h"
#line 1518 "/opt/microchip/mplabc18/v3.40/bin/../h/p18f4455.h"


#line 1522 "/opt/microchip/mplabc18/v3.40/bin/../h/p18f4455.h"
 
#line 1524 "/opt/microchip/mplabc18/v3.40/bin/../h/p18f4455.h"


#line 1527 "/opt/microchip/mplabc18/v3.40/bin/../h/p18f4455.h"
#line 1 "build_lights.c"

#line 1 "/opt/microchip/mplabc18/v3.40/bin/../h/delays.h"

#line 3 "/opt/microchip/mplabc18/v3.40/bin/../h/delays.h"


#line 13 "/opt/microchip/mplabc18/v3.40/bin/../h/delays.h"
 

 
#line 1 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"

#line 3 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"

#line 5 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 7 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 9 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 11 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 13 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 15 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 17 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 19 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 21 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 23 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 25 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 27 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 29 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 31 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 33 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 35 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 37 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 39 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 41 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 43 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 45 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 47 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 49 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 51 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 53 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 55 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 57 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 59 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 61 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 63 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 65 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 67 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 69 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 71 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 73 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 75 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 77 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 79 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 81 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 83 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 85 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 87 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 89 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 91 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 93 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 95 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 97 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 99 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 101 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 103 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 105 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 107 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 109 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 111 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 113 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 115 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 117 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 119 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 121 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 123 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 125 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 127 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 129 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 131 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 133 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 135 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 137 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 139 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 141 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 143 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 145 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 147 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 149 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 151 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 153 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 155 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 157 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 159 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 161 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 163 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 165 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 167 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 169 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 171 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 173 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 175 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 177 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 1 "/opt/microchip/mplabc18/v3.40/bin/../h/p18f4455.h"

#line 5 "/opt/microchip/mplabc18/v3.40/bin/../h/p18f4455.h"
 


#line 1501 "/opt/microchip/mplabc18/v3.40/bin/../h/p18f4455.h"

#line 1507 "/opt/microchip/mplabc18/v3.40/bin/../h/p18f4455.h"

#line 1522 "/opt/microchip/mplabc18/v3.40/bin/../h/p18f4455.h"
#line 1527 "/opt/microchip/mplabc18/v3.40/bin/../h/p18f4455.h"
#line 177 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"

#line 179 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 181 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 183 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 185 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 187 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 189 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 191 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 193 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 195 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 197 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 199 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 201 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 203 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 205 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 207 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 209 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 211 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 213 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 215 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 217 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 219 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 221 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 223 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 225 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 227 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 229 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 231 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 233 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 235 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 237 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 239 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 241 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 243 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 245 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 247 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 249 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 251 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 253 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 255 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 257 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 259 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 261 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 263 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 265 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 267 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 269 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 271 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 273 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 275 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 277 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 279 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 281 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 283 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 285 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 287 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 289 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 291 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 293 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 295 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 297 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 299 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 301 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 303 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 305 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 307 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 309 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 311 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 313 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 315 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 317 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 319 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 321 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 323 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 325 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 327 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 329 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 331 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 333 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 335 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 337 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 339 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 341 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 343 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 345 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 347 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 349 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 351 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 353 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 355 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 357 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 359 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 361 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 363 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 365 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 367 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 369 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 371 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 373 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 375 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 377 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 379 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 381 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 383 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 385 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 387 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 389 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 391 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 393 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 395 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 397 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 399 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 401 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 403 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 405 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 407 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 409 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 411 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 413 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 415 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 417 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 419 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 421 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 423 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 425 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 427 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 429 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 431 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 433 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 435 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 437 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 439 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 441 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 443 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 445 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 447 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 449 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 451 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 453 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 455 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 457 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 459 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 461 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 463 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 465 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 467 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 469 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 471 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 473 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 475 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 477 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 479 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 481 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 483 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 485 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 487 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 489 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 491 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 493 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 495 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 497 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 499 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 501 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 503 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 505 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 507 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 509 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 511 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 513 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 515 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 517 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 519 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 521 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 523 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 525 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 527 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 529 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 531 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 533 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 535 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 537 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 539 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 541 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 543 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 545 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 547 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 549 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 551 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 553 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 555 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 557 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 559 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 561 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"

#line 563 "/opt/microchip/mplabc18/v3.40/bin/../h/p18cxxx.h"
#line 16 "/opt/microchip/mplabc18/v3.40/bin/../h/delays.h"


 
#line 20 "/opt/microchip/mplabc18/v3.40/bin/../h/delays.h"

#line 22 "/opt/microchip/mplabc18/v3.40/bin/../h/delays.h"

 
#line 25 "/opt/microchip/mplabc18/v3.40/bin/../h/delays.h"


#line 31 "/opt/microchip/mplabc18/v3.40/bin/../h/delays.h"
 
void Delay10TCYx(auto  unsigned char);


#line 38 "/opt/microchip/mplabc18/v3.40/bin/../h/delays.h"
 
void Delay100TCYx(auto  unsigned char);


#line 45 "/opt/microchip/mplabc18/v3.40/bin/../h/delays.h"
 
void Delay1KTCYx(auto  unsigned char);


#line 52 "/opt/microchip/mplabc18/v3.40/bin/../h/delays.h"
 
void Delay10KTCYx(auto  unsigned char);

#line 56 "/opt/microchip/mplabc18/v3.40/bin/../h/delays.h"
#line 2 "build_lights.c"

#line 1 "./Headers/usb_config.h"

#line 23 "./Headers/usb_config.h"
 


#line 27 "./Headers/usb_config.h"


#line 30 "./Headers/usb_config.h"
#line 31 "./Headers/usb_config.h"
#line 32 "./Headers/usb_config.h"


#line 35 "./Headers/usb_config.h"
#line 36 "./Headers/usb_config.h"


#line 39 "./Headers/usb_config.h"
#line 40 "./Headers/usb_config.h"


#line 43 "./Headers/usb_config.h"



#line 47 "./Headers/usb_config.h"



#line 51 "./Headers/usb_config.h"



#line 55 "./Headers/usb_config.h"


#line 58 "./Headers/usb_config.h"



#line 62 "./Headers/usb_config.h"





#line 68 "./Headers/usb_config.h"

#line 70 "./Headers/usb_config.h"

#line 72 "./Headers/usb_config.h"



#line 76 "./Headers/usb_config.h"











#line 88 "./Headers/usb_config.h"


#line 91 "./Headers/usb_config.h"
#line 92 "./Headers/usb_config.h"
#line 93 "./Headers/usb_config.h"
#line 94 "./Headers/usb_config.h"
#line 95 "./Headers/usb_config.h"
#line 96 "./Headers/usb_config.h"

#line 98 "./Headers/usb_config.h"
#line 3 "build_lights.c"

#line 1 "/opt/microchip/libraries/Microchip/Include/USB/usb.h"


#line 7 "/opt/microchip/libraries/Microchip/Include/USB/usb.h"
 


#line 57 "/opt/microchip/libraries/Microchip/Include/USB/usb.h"
 

#line 85 "/opt/microchip/libraries/Microchip/Include/USB/usb.h"
 




#line 96 "/opt/microchip/libraries/Microchip/Include/USB/usb.h"
 



#line 101 "/opt/microchip/libraries/Microchip/Include/USB/usb.h"









#line 1 "/opt/microchip/mplabc18/v3.40/bin/../h/GenericTypeDefs.h"

#line 45 "/opt/microchip/mplabc18/v3.40/bin/../h/GenericTypeDefs.h"
 


#line 49 "/opt/microchip/mplabc18/v3.40/bin/../h/GenericTypeDefs.h"

 
#line 52 "/opt/microchip/mplabc18/v3.40/bin/../h/GenericTypeDefs.h"
#line 54 "/opt/microchip/mplabc18/v3.40/bin/../h/GenericTypeDefs.h"
#line 55 "/opt/microchip/mplabc18/v3.40/bin/../h/GenericTypeDefs.h"
#line 56 "/opt/microchip/mplabc18/v3.40/bin/../h/GenericTypeDefs.h"

#line 58 "/opt/microchip/mplabc18/v3.40/bin/../h/GenericTypeDefs.h"
#line 59 "/opt/microchip/mplabc18/v3.40/bin/../h/GenericTypeDefs.h"
#line 60 "/opt/microchip/mplabc18/v3.40/bin/../h/GenericTypeDefs.h"

 
#line 1 "/opt/microchip/mplabc18/v3.40/bin/../h/stddef.h"
 

#line 4 "/opt/microchip/mplabc18/v3.40/bin/../h/stddef.h"

typedef unsigned char wchar_t;


#line 10 "/opt/microchip/mplabc18/v3.40/bin/../h/stddef.h"
 
typedef signed short int ptrdiff_t;
typedef signed short int ptrdiffram_t;
typedef signed short long int ptrdiffrom_t;


#line 20 "/opt/microchip/mplabc18/v3.40/bin/../h/stddef.h"
 
typedef unsigned short int size_t;
typedef unsigned short int sizeram_t;
typedef unsigned short long int sizerom_t;


#line 34 "/opt/microchip/mplabc18/v3.40/bin/../h/stddef.h"
 
#line 36 "/opt/microchip/mplabc18/v3.40/bin/../h/stddef.h"


#line 41 "/opt/microchip/mplabc18/v3.40/bin/../h/stddef.h"
 
#line 43 "/opt/microchip/mplabc18/v3.40/bin/../h/stddef.h"

#line 45 "/opt/microchip/mplabc18/v3.40/bin/../h/stddef.h"
#line 62 "/opt/microchip/mplabc18/v3.40/bin/../h/GenericTypeDefs.h"
 

typedef enum _BOOL { FALSE = 0, TRUE } BOOL;     
typedef enum _BIT { CLEAR = 0, SET } BIT;

#line 68 "/opt/microchip/mplabc18/v3.40/bin/../h/GenericTypeDefs.h"
#line 69 "/opt/microchip/mplabc18/v3.40/bin/../h/GenericTypeDefs.h"
#line 70 "/opt/microchip/mplabc18/v3.40/bin/../h/GenericTypeDefs.h"

 
typedef signed int          INT;
typedef signed char         INT8;
typedef signed short int    INT16;
typedef signed long int     INT32;

 
#line 79 "/opt/microchip/mplabc18/v3.40/bin/../h/GenericTypeDefs.h"
#line 81 "/opt/microchip/mplabc18/v3.40/bin/../h/GenericTypeDefs.h"

 
typedef unsigned int        UINT;
typedef unsigned char       UINT8;
typedef unsigned short int  UINT16;
 
#line 88 "/opt/microchip/mplabc18/v3.40/bin/../h/GenericTypeDefs.h"
typedef unsigned short long UINT24;
#line 90 "/opt/microchip/mplabc18/v3.40/bin/../h/GenericTypeDefs.h"
typedef unsigned long int   UINT32;      
 
#line 93 "/opt/microchip/mplabc18/v3.40/bin/../h/GenericTypeDefs.h"
#line 95 "/opt/microchip/mplabc18/v3.40/bin/../h/GenericTypeDefs.h"

typedef union
{
    UINT8 Val;
    struct
    {
         UINT8 b0:1;
         UINT8 b1:1;
         UINT8 b2:1;
         UINT8 b3:1;
         UINT8 b4:1;
         UINT8 b5:1;
         UINT8 b6:1;
         UINT8 b7:1;
    } bits;
} UINT8_VAL, UINT8_BITS;

typedef union 
{
    UINT16 Val;
    UINT8 v[2] ;
    struct 
    {
        UINT8 LB;
        UINT8 HB;
    } byte;
    struct 
    {
         UINT8 b0:1;
         UINT8 b1:1;
         UINT8 b2:1;
         UINT8 b3:1;
         UINT8 b4:1;
         UINT8 b5:1;
         UINT8 b6:1;
         UINT8 b7:1;
         UINT8 b8:1;
         UINT8 b9:1;
         UINT8 b10:1;
         UINT8 b11:1;
         UINT8 b12:1;
         UINT8 b13:1;
         UINT8 b14:1;
         UINT8 b15:1;
    } bits;
} UINT16_VAL, UINT16_BITS;

 
#line 144 "/opt/microchip/mplabc18/v3.40/bin/../h/GenericTypeDefs.h"
typedef union
{
    UINT24 Val;
    UINT8 v[3] ;
    struct 
    {
        UINT8 LB;
        UINT8 HB;
        UINT8 UB;
    } byte;
    struct 
    {
         UINT8 b0:1;
         UINT8 b1:1;
         UINT8 b2:1;
         UINT8 b3:1;
         UINT8 b4:1;
         UINT8 b5:1;
         UINT8 b6:1;
         UINT8 b7:1;
         UINT8 b8:1;
         UINT8 b9:1;
         UINT8 b10:1;
         UINT8 b11:1;
         UINT8 b12:1;
         UINT8 b13:1;
         UINT8 b14:1;
         UINT8 b15:1;
         UINT8 b16:1;
         UINT8 b17:1;
         UINT8 b18:1;
         UINT8 b19:1;
         UINT8 b20:1;
         UINT8 b21:1;
         UINT8 b22:1;
         UINT8 b23:1;
    } bits;
} UINT24_VAL, UINT24_BITS;
#line 183 "/opt/microchip/mplabc18/v3.40/bin/../h/GenericTypeDefs.h"

typedef union
{
    UINT32 Val;
    UINT16 w[2] ;
    UINT8  v[4] ;
    struct 
    {
        UINT16 LW;
        UINT16 HW;
    } word;
    struct 
    {
        UINT8 LB;
        UINT8 HB;
        UINT8 UB;
        UINT8 MB;
    } byte;
    struct 
    {
        UINT16_VAL low;
        UINT16_VAL high;
    }wordUnion;
    struct 
    {
         UINT8 b0:1;
         UINT8 b1:1;
         UINT8 b2:1;
         UINT8 b3:1;
         UINT8 b4:1;
         UINT8 b5:1;
         UINT8 b6:1;
         UINT8 b7:1;
         UINT8 b8:1;
         UINT8 b9:1;
         UINT8 b10:1;
         UINT8 b11:1;
         UINT8 b12:1;
         UINT8 b13:1;
         UINT8 b14:1;
         UINT8 b15:1;
         UINT8 b16:1;
         UINT8 b17:1;
         UINT8 b18:1;
         UINT8 b19:1;
         UINT8 b20:1;
         UINT8 b21:1;
         UINT8 b22:1;
         UINT8 b23:1;
         UINT8 b24:1;
         UINT8 b25:1;
         UINT8 b26:1;
         UINT8 b27:1;
         UINT8 b28:1;
         UINT8 b29:1;
         UINT8 b30:1;
         UINT8 b31:1;
    } bits;
} UINT32_VAL;

 
#line 245 "/opt/microchip/mplabc18/v3.40/bin/../h/GenericTypeDefs.h"
#line 332 "/opt/microchip/mplabc18/v3.40/bin/../h/GenericTypeDefs.h"

 

 
typedef void                    VOID;

typedef char                    CHAR8;
typedef unsigned char           UCHAR8;

typedef unsigned char           BYTE;                            
typedef unsigned short int      WORD;                            
typedef unsigned long           DWORD;                           
 

typedef unsigned long long      QWORD;                           
typedef signed char             CHAR;                            
typedef signed short int        SHORT;                           
typedef signed long             LONG;                            
 

typedef signed long long        LONGLONG;                        
typedef union
{
    BYTE Val;
    struct 
    {
         BYTE b0:1;
         BYTE b1:1;
         BYTE b2:1;
         BYTE b3:1;
         BYTE b4:1;
         BYTE b5:1;
         BYTE b6:1;
         BYTE b7:1;
    } bits;
} BYTE_VAL, BYTE_BITS;

typedef union
{
    WORD Val;
    BYTE v[2] ;
    struct 
    {
        BYTE LB;
        BYTE HB;
    } byte;
    struct 
    {
         BYTE b0:1;
         BYTE b1:1;
         BYTE b2:1;
         BYTE b3:1;
         BYTE b4:1;
         BYTE b5:1;
         BYTE b6:1;
         BYTE b7:1;
         BYTE b8:1;
         BYTE b9:1;
         BYTE b10:1;
         BYTE b11:1;
         BYTE b12:1;
         BYTE b13:1;
         BYTE b14:1;
         BYTE b15:1;
    } bits;
} WORD_VAL, WORD_BITS;

typedef union
{
    DWORD Val;
    WORD w[2] ;
    BYTE v[4] ;
    struct 
    {
        WORD LW;
        WORD HW;
    } word;
    struct 
    {
        BYTE LB;
        BYTE HB;
        BYTE UB;
        BYTE MB;
    } byte;
    struct 
    {
        WORD_VAL low;
        WORD_VAL high;
    }wordUnion;
    struct 
    {
         BYTE b0:1;
         BYTE b1:1;
         BYTE b2:1;
         BYTE b3:1;
         BYTE b4:1;
         BYTE b5:1;
         BYTE b6:1;
         BYTE b7:1;
         BYTE b8:1;
         BYTE b9:1;
         BYTE b10:1;
         BYTE b11:1;
         BYTE b12:1;
         BYTE b13:1;
         BYTE b14:1;
         BYTE b15:1;
         BYTE b16:1;
         BYTE b17:1;
         BYTE b18:1;
         BYTE b19:1;
         BYTE b20:1;
         BYTE b21:1;
         BYTE b22:1;
         BYTE b23:1;
         BYTE b24:1;
         BYTE b25:1;
         BYTE b26:1;
         BYTE b27:1;
         BYTE b28:1;
         BYTE b29:1;
         BYTE b30:1;
         BYTE b31:1;
    } bits;
} DWORD_VAL;

 
typedef union
{
    QWORD Val;
    DWORD d[2] ;
    WORD w[4] ;
    BYTE v[8] ;
    struct 
    {
        DWORD LD;
        DWORD HD;
    } dword;
    struct 
    {
        WORD LW;
        WORD HW;
        WORD UW;
        WORD MW;
    } word;
    struct 
    {
         BYTE b0:1;
         BYTE b1:1;
         BYTE b2:1;
         BYTE b3:1;
         BYTE b4:1;
         BYTE b5:1;
         BYTE b6:1;
         BYTE b7:1;
         BYTE b8:1;
         BYTE b9:1;
         BYTE b10:1;
         BYTE b11:1;
         BYTE b12:1;
         BYTE b13:1;
         BYTE b14:1;
         BYTE b15:1;
         BYTE b16:1;
         BYTE b17:1;
         BYTE b18:1;
         BYTE b19:1;
         BYTE b20:1;
         BYTE b21:1;
         BYTE b22:1;
         BYTE b23:1;
         BYTE b24:1;
         BYTE b25:1;
         BYTE b26:1;
         BYTE b27:1;
         BYTE b28:1;
         BYTE b29:1;
         BYTE b30:1;
         BYTE b31:1;
         BYTE b32:1;
         BYTE b33:1;
         BYTE b34:1;
         BYTE b35:1;
         BYTE b36:1;
         BYTE b37:1;
         BYTE b38:1;
         BYTE b39:1;
         BYTE b40:1;
         BYTE b41:1;
         BYTE b42:1;
         BYTE b43:1;
         BYTE b44:1;
         BYTE b45:1;
         BYTE b46:1;
         BYTE b47:1;
         BYTE b48:1;
         BYTE b49:1;
         BYTE b50:1;
         BYTE b51:1;
         BYTE b52:1;
         BYTE b53:1;
         BYTE b54:1;
         BYTE b55:1;
         BYTE b56:1;
         BYTE b57:1;
         BYTE b58:1;
         BYTE b59:1;
         BYTE b60:1;
         BYTE b61:1;
         BYTE b62:1;
         BYTE b63:1;
    } bits;
} QWORD_VAL;

#line 547 "/opt/microchip/mplabc18/v3.40/bin/../h/GenericTypeDefs.h"

#line 549 "/opt/microchip/mplabc18/v3.40/bin/../h/GenericTypeDefs.h"
#line 110 "/opt/microchip/libraries/Microchip/Include/USB/usb.h"



         
            

#line 117 "/opt/microchip/libraries/Microchip/Include/USB/usb.h"
     
#line 119 "/opt/microchip/libraries/Microchip/Include/USB/usb.h"

#line 121 "/opt/microchip/libraries/Microchip/Include/USB/usb.h"
#line 123 "/opt/microchip/libraries/Microchip/Include/USB/usb.h"

#line 125 "/opt/microchip/libraries/Microchip/Include/USB/usb.h"
#line 127 "/opt/microchip/libraries/Microchip/Include/USB/usb.h"

            







#line 137 "/opt/microchip/libraries/Microchip/Include/USB/usb.h"
#line 138 "/opt/microchip/libraries/Microchip/Include/USB/usb.h"
#line 139 "/opt/microchip/libraries/Microchip/Include/USB/usb.h"

#line 141 "/opt/microchip/libraries/Microchip/Include/USB/usb.h"

#line 143 "/opt/microchip/libraries/Microchip/Include/USB/usb.h"
 

#line 4 "build_lights.c"

#line 1 "/opt/microchip/libraries/Microchip/Include/USB/usb_function_hid.h"

#line 99 "/opt/microchip/libraries/Microchip/Include/USB/usb_function_hid.h"
 

#line 102 "/opt/microchip/libraries/Microchip/Include/USB/usb_function_hid.h"


 

 

 
#line 110 "/opt/microchip/libraries/Microchip/Include/USB/usb_function_hid.h"
#line 111 "/opt/microchip/libraries/Microchip/Include/USB/usb_function_hid.h"
#line 112 "/opt/microchip/libraries/Microchip/Include/USB/usb_function_hid.h"
#line 113 "/opt/microchip/libraries/Microchip/Include/USB/usb_function_hid.h"
#line 114 "/opt/microchip/libraries/Microchip/Include/USB/usb_function_hid.h"
#line 115 "/opt/microchip/libraries/Microchip/Include/USB/usb_function_hid.h"

 
#line 118 "/opt/microchip/libraries/Microchip/Include/USB/usb_function_hid.h"
#line 119 "/opt/microchip/libraries/Microchip/Include/USB/usb_function_hid.h"
#line 120 "/opt/microchip/libraries/Microchip/Include/USB/usb_function_hid.h"

 
#line 123 "/opt/microchip/libraries/Microchip/Include/USB/usb_function_hid.h"
#line 124 "/opt/microchip/libraries/Microchip/Include/USB/usb_function_hid.h"

 
#line 127 "/opt/microchip/libraries/Microchip/Include/USB/usb_function_hid.h"

 
#line 130 "/opt/microchip/libraries/Microchip/Include/USB/usb_function_hid.h"

 
#line 133 "/opt/microchip/libraries/Microchip/Include/USB/usb_function_hid.h"
#line 134 "/opt/microchip/libraries/Microchip/Include/USB/usb_function_hid.h"
#line 135 "/opt/microchip/libraries/Microchip/Include/USB/usb_function_hid.h"


#line 173 "/opt/microchip/libraries/Microchip/Include/USB/usb_function_hid.h"
 
void USBCheckHIDRequest(void);


#line 224 "/opt/microchip/libraries/Microchip/Include/USB/usb_function_hid.h"
 
#line 226 "/opt/microchip/libraries/Microchip/Include/USB/usb_function_hid.h"


#line 273 "/opt/microchip/libraries/Microchip/Include/USB/usb_function_hid.h"
 
#line 275 "/opt/microchip/libraries/Microchip/Include/USB/usb_function_hid.h"


#line 313 "/opt/microchip/libraries/Microchip/Include/USB/usb_function_hid.h"
 
#line 315 "/opt/microchip/libraries/Microchip/Include/USB/usb_function_hid.h"


#line 349 "/opt/microchip/libraries/Microchip/Include/USB/usb_function_hid.h"
 
#line 351 "/opt/microchip/libraries/Microchip/Include/USB/usb_function_hid.h"





typedef struct _USB_HID_DSC_HEADER
{
    BYTE bDescriptorType;	
    WORD wDscLength;		
} USB_HID_DSC_HEADER;



typedef struct _USB_HID_DSC
{
    BYTE bLength;			
	BYTE bDescriptorType;	
	WORD bcdHID;			
    BYTE bCountryCode;		
	BYTE bNumDsc;			


    
     
    
} USB_HID_DSC;

 
extern volatile CTRL_TRF_SETUP SetupPkt;
extern ROM BYTE configDescriptor1[];
extern volatile BYTE CtrlTrfData[8 ];

#line 384 "/opt/microchip/libraries/Microchip/Include/USB/usb_function_hid.h"
extern ROM struct{BYTE report[28 ];}hid_rpt01;
#line 386 "/opt/microchip/libraries/Microchip/Include/USB/usb_function_hid.h"

#line 388 "/opt/microchip/libraries/Microchip/Include/USB/usb_function_hid.h"
#line 5 "build_lights.c"


#line 1 "./Headers/HardwareProfile.h"

#line 23 "./Headers/HardwareProfile.h"
 


#line 27 "./Headers/HardwareProfile.h"








#line 36 "./Headers/HardwareProfile.h"
#line 37 "./Headers/HardwareProfile.h"
#line 39 "./Headers/HardwareProfile.h"
#line 40 "./Headers/HardwareProfile.h"
#line 41 "./Headers/HardwareProfile.h"




#line 46 "./Headers/HardwareProfile.h"
#line 47 "./Headers/HardwareProfile.h"
#line 49 "./Headers/HardwareProfile.h"
#line 50 "./Headers/HardwareProfile.h"
#line 51 "./Headers/HardwareProfile.h"





#line 57 "./Headers/HardwareProfile.h"




#line 62 "./Headers/HardwareProfile.h"


#line 65 "./Headers/HardwareProfile.h"


#line 68 "./Headers/HardwareProfile.h"


#line 71 "./Headers/HardwareProfile.h"
#line 72 "./Headers/HardwareProfile.h"


#line 75 "./Headers/HardwareProfile.h"
#line 76 "./Headers/HardwareProfile.h"


#line 79 "./Headers/HardwareProfile.h"
#line 80 "./Headers/HardwareProfile.h"


#line 83 "./Headers/HardwareProfile.h"
#line 84 "./Headers/HardwareProfile.h"
#line 85 "./Headers/HardwareProfile.h"
#line 86 "./Headers/HardwareProfile.h"









#line 96 "./Headers/HardwareProfile.h"

#line 98 "./Headers/HardwareProfile.h"
#line 7 "build_lights.c"



#pragma udata
#pragma udata USB_VARIABLES=0x500
unsigned char ReceivedDataBuffer[64];
unsigned char ToSendDataBuffer[64];
#pragma udata

USB_HANDLE USBOutHandle = 0;
USB_HANDLE USBInHandle = 0;


#pragma config PLLDIV   = 5         
#pragma config CPUDIV   = OSC1_PLL2
#pragma config USBDIV   = 2         
#pragma config FOSC     = HSPLL_HS
#pragma config FCMEN    = OFF
#pragma config IESO     = OFF
#pragma config PWRT     = OFF
#pragma config BOR      = ON
#pragma config BORV     = 3
#pragma config VREGEN   = ON
#pragma config WDT      = OFF
#pragma config WDTPS    = 32768
#pragma config MCLRE    = OFF
#pragma config LPT1OSC  = OFF
#pragma config PBADEN   = OFF

#pragma config STVREN   = ON
#pragma config LVP      = OFF

#pragma config XINST    = OFF
#pragma config CP0      = OFF
#pragma config CP1      = OFF


#pragma config CPB      = OFF

#pragma config WRT0     = OFF
#pragma config WRT1     = OFF


#pragma config WRTB     = OFF
#pragma config WRTC     = OFF

#pragma config EBTR0    = OFF
#pragma config EBTR1    = OFF


#pragma config EBTRB    = OFF

void processUsbCommands(void);
void USBCBSendResume(void);
void highPriorityISRCode();
void lowPriorityISRCode();


#line 66 "build_lights.c"
#line 67 "build_lights.c"
#line 68 "build_lights.c"
#line 69 "build_lights.c"
#line 70 "build_lights.c"
#line 74 "build_lights.c"
#line 78 "build_lights.c"

#line 80 "build_lights.c"
	extern void _startup (void);
	#pragma code REMAPPED_RESET_VECTOR = 0x1000 
	void _reset (void)
	{
	    _asm goto _startup _endasm
	}
#line 87 "build_lights.c"

#pragma code REMAPPED_HIGH_INTERRUPT_VECTOR = 0x1008 
void Remapped_High_ISR (void)
{
     _asm goto highPriorityISRCode _endasm
}

#pragma code REMAPPED_LOW_INTERRUPT_VECTOR = 0x1018 
void Remapped_Low_ISR (void)
{
     _asm goto lowPriorityISRCode _endasm
}

#line 101 "build_lights.c"
#pragma code HIGH_INTERRUPT_VECTOR = 0x08
void High_ISR (void)
{
     _asm goto 0x1008  _endasm
}

#pragma code LOW_INTERRUPT_VECTOR = 0x18
void Low_ISR (void)
{
     _asm goto 0x1018  _endasm
}
#line 113 "build_lights.c"

#pragma code

int volatile servoOn = 0;
int volatile positions[3][2][2] = {{{{0b11111010},{0b00100011}},{{0b00011011},{0b01111011}}},
							           {{{0b11101111},{0b10010111}},{{0b00100110},{0b00000111}}},
							           {{{0b11100011},{0b11011111}},{{0b00110001},{0b10111111}}}};
int volatile position = 0;


#pragma interrupt highPriorityISRCode
void highPriorityISRCode()
{
	

	if (INTCONbits.TMR0IF == 1) {
		INTCONbits.TMR0IF = 0;
		if (servoOn == 0) {
			servoOn = 1;
			PORTEbits.RE0 = 1;
			TMR0H = positions[position][0][0];
			TMR0L = positions[position][0][1];
		} else {
			servoOn = 0;
			PORTEbits.RE0 = 0;
			TMR0H = positions[position][1][0];
			TMR0L = positions[position][1][1];
		}
	}

#line 144 "build_lights.c"
#line 147 "build_lights.c"

}


int volatile dimCountPerInc = 0;       
int volatile dimCounter = 0;        
int volatile dimLightOn = 0;      
int volatile dimTogglePoint = 0;  
int volatile dimInterval = 0;     
int volatile dimIntervalCounter = 0;     
int volatile dimOff = 1;
int volatile dimLightBits = 0;


#pragma interruptlow lowPriorityISRCode
void lowPriorityISRCode()
{
	
  if (PIR1bits.TMR1IF == 1) {
    PIR1bits.TMR1IF = 0;

    if (dimOff == 0) {

      if (dimIntervalCounter != 0 && dimTogglePoint == dimIntervalCounter) {
        dimLightOn = dimLightOn == 0 ? 1 : 0;
        
      }

      if (dimIntervalCounter == dimInterval) {

        if (dimTogglePoint != 0 && dimTogglePoint != dimInterval) {
          dimLightOn = dimLightOn == 0 ? 1 : 0;
          
        }

        dimIntervalCounter = 0;
        dimCounter++;

        if (dimCounter == dimCountPerInc) {

          dimTogglePoint++;
          dimCounter = 0;

          if (dimTogglePoint == dimInterval) {
            dimOff = 1;
          }

        }
      }
      else {
        dimIntervalCounter++;
      }
    }

  }
}

void main (void)
{

  T0CONbits.TMR0ON = 0;		
  T1CONbits.TMR1ON = 0;

  TRISA = 0;
  TRISB = 0;
  TRISC = 0;
  TRISD = 0;
  TRISE = 0;
  PORTA = 0;
  PORTB = 0;
  PORTC = 0;
  PORTD = 0;
  PORTE = 0;

#line 222 "build_lights.c"
#line 224 "build_lights.c"

  
  
  
  
  
  
#line 232 "build_lights.c"
#line 234 "build_lights.c"

  USBOutHandle = 0;
  USBInHandle = 0;

#line 239 "build_lights.c"
#line 241 "build_lights.c"

  USBDeviceInit();

  RCONbits.IPEN = 1;			
  INTCONbits.GIE_GIEH = 1;	
  INTCONbits.TMR0IE = 1;		
  INTCON2bits.TMR0IP = 1;		
  PIE1bits.TMR1IE = 1;		
  IPR1bits.TMR1IP = 0;		

  
  T0CONbits.T08BIT = 0;
  T0CONbits.T0CS = 0;
  T0CONbits.PSA = 0;			
  T0CONbits.T0PS2 = 0;
  T0CONbits.T0PS1 = 0;
  T0CONbits.T0PS0 = 1;

  
  T1CONbits.RD16 = 0;
  T1CONbits.T1RUN = 1;
  T1CONbits.T1CKPS0 = 1;
  T1CONbits.T1CKPS1 = 1;
  T1CONbits.T1OSCEN = 1;
  T1CONbits.TMR1CS = 0;

  T0CONbits.TMR0ON = 1;
  T1CONbits.TMR1ON = 1;

  PORTBbits.RB3 = 0;
  PORTBbits.RB4 = 0;

  PORTA = 0b00000001;

  
  while(1)
  {
#line 279 "build_lights.c"
      
      
      USBDeviceTasks();
#line 283 "build_lights.c"

    
    processUsbCommands();

  }

}

int lightOn = 0;
int lightPD = 0;
int lightPDLow = 0;
int lightPDHi = 0;
int lightPC = 0;


void processUsbCommands(void)
{
  if(USBDeviceState <= ATTACHED_STATE)
  {

  }
  if(USBDeviceState <= POWERED_STATE)
  {
  }

  
  if((USBDeviceState < CONFIGURED_STATE) || (USBSuspendControl == 1))
  {
    
    return;
  }

  if (lightOn) {
    PORTA = 0b00100011;
  } else {
    PORTA = 0b00000011;
  }

	
  if(!USBHandleBusy(USBOutHandle) )
  {
    switch(ReceivedDataBuffer[0])
	{
      case 0x69:
      {
	      ToSendDataBuffer[0] = 'H';
        ToSendDataBuffer[1] = 'e';
        ToSendDataBuffer[2] = 'l';
        ToSendDataBuffer[3] = 'l';
        ToSendDataBuffer[4] = 'o';

        
        if(!USBHandleBusy(USBInHandle) )
		    {
          USBInHandle = USBTxOnePacket (1 ,(BYTE*)&ToSendDataBuffer[0],64);
		    }
        break;
      }
      case 0x50:  
      {
	    	position = ReceivedDataBuffer[1];
        break;
      }
      case 0x4C:  
      {
		    lightOn = ReceivedDataBuffer[1];
        if (lightOn == 1) {
          PORTA = 0b00100011;
        } else {
          PORTA = 0b00000011;
        }
      }
      case 0x4D:
      {
        
#line 370 "build_lights.c"
 

        
        
        
        

        dimLightBits = ReceivedDataBuffer[1];
        dimInterval = (ReceivedDataBuffer[2] >> 7) | ReceivedDataBuffer[3];
        dimLightOn = ReceivedDataBuffer[4];
        dimCountPerInc = ReceivedDataBuffer[5];
        dimOff = 0;
        dimIntervalCounter = 0;
        dimTogglePoint = 0;
        dimCounter = 0;
      }

      default:	
        break;
	}

    
    USBOutHandle = USBRxOnePacket (1 ,(BYTE*)&ReceivedDataBuffer,64);
	}
}




void USBCBSuspend(void)
{
}


void USBCBWakeFromSuspend(void)
{
}


void USBCB_SOF_Handler(void)
{
  
}



void USBCBErrorHandler(void)
{
  
  
}


void USBCBCheckOtherReq(void)
{
  USBCheckHIDRequest();
}


void USBCBStdSetDscHandler(void)
{
  
}


void USBCBInitEP(void)
{
  
  USBEnableEndpoint(1 ,USB_IN_ENABLED|USB_OUT_ENABLED|USB_HANDSHAKE_ENABLED|USB_DISALLOW_SETUP);

  
  USBOutHandle = USBRxOnePacket (1 ,(BYTE*)&ReceivedDataBuffer,64);
}


void USBCBSendResume(void)
{
  static WORD delay_count;

  
  if(USBGetRemoteWakeupStatus() == 1 )
  {
    
    if(USBIsBusSuspended() == 1 )
    {
      USBMaskInterrupts();

      
      USBCBWakeFromSuspend();
      USBSuspendControl = 0;
      USBBusIsSuspended = 0 ;

      
      
      
      
      
      
      delay_count = 3600U;
      do
      {
        delay_count--;
      } while(delay_count);

      
      USBResumeControl = 1;
      delay_count = 1800U;
      do
      {
        delay_count--;
      } while(delay_count);
      USBResumeControl = 0;

      USBUnmaskInterrupts();
    }
  }
}


BOOL USER_USB_CALLBACK_EVENT_HANDLER(USB_EVENT event, void *pdata, WORD size)
{
  switch(event)
  {
    case EVENT_TRANSFER:
      
      break;
    case EVENT_SOF:
      USBCB_SOF_Handler();
      break;
    case EVENT_SUSPEND:
      USBCBSuspend();
      break;
    case EVENT_RESUME:
      USBCBWakeFromSuspend();
      break;
    case EVENT_CONFIGURED:
      USBCBInitEP();
      break;
    case EVENT_SET_DESCRIPTOR:
      USBCBStdSetDscHandler();
      break;
    case EVENT_EP0_REQUEST:
      USBCBCheckOtherReq();
      break;
    case EVENT_BUS_ERROR:
      USBCBErrorHandler();
      break;
    default:
      break;
  }
  return 1 ;
}

