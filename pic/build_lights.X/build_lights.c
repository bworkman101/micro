#include <p18f4455.h>
#include <delays.h>
#include "Headers/usb_config.h"
#include "usb.h"
#include "usb_function_hid.h"

#include "Headers/HardwareProfile.h"

// Define the globals for the USB data in the USB RAM of the PIC18F*550
#pragma udata
#pragma udata USB_VARIABLES=0x500
unsigned char ReceivedDataBuffer[64];
unsigned char ToSendDataBuffer[64];
#pragma udata

USB_HANDLE USBOutHandle = 0;
USB_HANDLE USBInHandle = 0;

// PIC18F4550/PIC18F2550 configuration for the WFF Generic HID test device
#pragma config PLLDIV   = 5         // 20Mhz external oscillator
#pragma config CPUDIV   = OSC1_PLL2
#pragma config USBDIV   = 2         // Clock source from 96MHz PLL/2
#pragma config FOSC     = HSPLL_HS
#pragma config FCMEN    = OFF
#pragma config IESO     = OFF
#pragma config PWRT     = OFF
#pragma config BOR      = ON
#pragma config BORV     = 3
#pragma config VREGEN   = ON
#pragma config WDT      = OFF
#pragma config WDTPS    = 32768
#pragma config MCLRE    = OFF
#pragma config LPT1OSC  = OFF
#pragma config PBADEN   = OFF
// #pragma config CCP2MX   = ON
#pragma config STVREN   = ON
#pragma config LVP      = OFF
// #pragma config ICPRT    = OFF
#pragma config XINST    = OFF
#pragma config CP0      = OFF
#pragma config CP1      = OFF
// #pragma config CP2      = OFF
// #pragma config CP3      = OFF
#pragma config CPB      = OFF
// #pragma config CPD      = OFF
#pragma config WRT0     = OFF
#pragma config WRT1     = OFF
// #pragma config WRT2     = OFF
// #pragma config WRT3     = OFF
#pragma config WRTB     = OFF
#pragma config WRTC     = OFF
// #pragma config WRTD     = OFF
#pragma config EBTR0    = OFF
#pragma config EBTR1    = OFF
// #pragma config EBTR2    = OFF
// #pragma config EBTR3    = OFF
#pragma config EBTRB    = OFF

void processUsbCommands(void);
void USBCBSendResume(void);
void highPriorityISRCode();
void lowPriorityISRCode();

// Remap vectors for compatibilty with Microchip USB boot loaders
#if defined(PROGRAMMABLE_WITH_USB_HID_BOOTLOADER)
	#define REMAPPED_RESET_VECTOR_ADDRESS			0x1000
	#define REMAPPED_HIGH_INTERRUPT_VECTOR_ADDRESS	0x1008
	#define REMAPPED_LOW_INTERRUPT_VECTOR_ADDRESS	0x1018
#elif defined(PROGRAMMABLE_WITH_USB_MCHPUSB_BOOTLOADER)
	#define REMAPPED_RESET_VECTOR_ADDRESS			0x800
	#define REMAPPED_HIGH_INTERRUPT_VECTOR_ADDRESS	0x808
	#define REMAPPED_LOW_INTERRUPT_VECTOR_ADDRESS	0x818
#else
	#define REMAPPED_RESET_VECTOR_ADDRESS			0x00
	#define REMAPPED_HIGH_INTERRUPT_VECTOR_ADDRESS	0x08
	#define REMAPPED_LOW_INTERRUPT_VECTOR_ADDRESS	0x18
#endif

#if defined(PROGRAMMABLE_WITH_USB_HID_BOOTLOADER) || defined(PROGRAMMABLE_WITH_USB_MCHPUSB_BOOTLOADER)
	extern void _startup (void);
	#pragma code REMAPPED_RESET_VECTOR = REMAPPED_RESET_VECTOR_ADDRESS
	void _reset (void)
	{
	    _asm goto _startup _endasm
	}
#endif

#pragma code REMAPPED_HIGH_INTERRUPT_VECTOR = REMAPPED_HIGH_INTERRUPT_VECTOR_ADDRESS
void Remapped_High_ISR (void)
{
     _asm goto highPriorityISRCode _endasm
}

#pragma code REMAPPED_LOW_INTERRUPT_VECTOR = REMAPPED_LOW_INTERRUPT_VECTOR_ADDRESS
void Remapped_Low_ISR (void)
{
     _asm goto lowPriorityISRCode _endasm
}

#if defined(PROGRAMMABLE_WITH_USB_HID_BOOTLOADER) || defined(PROGRAMMABLE_WITH_USB_MCHPUSB_BOOTLOADER)
#pragma code HIGH_INTERRUPT_VECTOR = 0x08
void High_ISR (void)
{
     _asm goto REMAPPED_HIGH_INTERRUPT_VECTOR_ADDRESS _endasm
}

#pragma code LOW_INTERRUPT_VECTOR = 0x18
void Low_ISR (void)
{
     _asm goto REMAPPED_LOW_INTERRUPT_VECTOR_ADDRESS _endasm
}
#endif

#pragma code

int volatile servoOn = 0;
int volatile positions[3][2][2] = {{{{0b11111010},{0b00100011}},{{0b00011011},{0b01111011}}},
							           {{{0b11101111},{0b10010111}},{{0b00100110},{0b00000111}}},
							           {{{0b11100011},{0b11011111}},{{0b00110001},{0b10111111}}}};
int volatile position = 0;

// High-priority ISR handling function
#pragma interrupt highPriorityISRCode
void highPriorityISRCode()
{
	// Application specific high-priority ISR code goes here

	if (INTCONbits.TMR0IF == 1) {
		INTCONbits.TMR0IF = 0;
		if (servoOn == 0) {
			servoOn = 1;
			PORTEbits.RE0 = 1;
			TMR0H = positions[position][0][0];
			TMR0L = positions[position][0][1];
		} else {
			servoOn = 0;
			PORTEbits.RE0 = 0;
			TMR0H = positions[position][1][0];
			TMR0L = positions[position][1][1];
		}
	}

	#if defined(USB_INTERRUPT)
		// Perform USB device tasks
		USBDeviceTasks();
	#endif

}

// int volatile dimInteruptsPerSec = 1500000;
int volatile dimCountPerInc = 0;       // what to count to before doing an increment
int volatile dimCounter = 0;        // counts to dimCountPerInc
int volatile dimLightOn = 0;      // toggles the light on and off
int volatile dimTogglePoint = 0;  // when to switch the dimming light on or off
int volatile dimInterval = 0;     // the dimming interval
int volatile dimIntervalCounter = 0;     // the dimming interval counter
int volatile dimOff = 1;
int volatile dimLightBits = 0;

// Low-priority ISR handling function
#pragma interruptlow lowPriorityISRCode
void lowPriorityISRCode()
{
	// Application specific low-priority ISR code goes here
  if (PIR1bits.TMR1IF == 1) {
    PIR1bits.TMR1IF = 0;

    if (dimOff == 0) {

      if (dimIntervalCounter != 0 && dimTogglePoint == dimIntervalCounter) {
        dimLightOn = dimLightOn == 0 ? 1 : 0;
        // set the light ports
      }

      if (dimIntervalCounter == dimInterval) {

        if (dimTogglePoint != 0 && dimTogglePoint != dimInterval) {
          dimLightOn = dimLightOn == 0 ? 1 : 0;
          // set the light ports
        }

        dimIntervalCounter = 0;
        dimCounter++;

        if (dimCounter == dimCountPerInc) {

          dimTogglePoint++;
          dimCounter = 0;

          if (dimTogglePoint == dimInterval) {
            dimOff = 1;
          }

        }
      }
      else {
        dimIntervalCounter++;
      }
    }

  }
}

void main (void)
{

  T0CONbits.TMR0ON = 0;		//turn off timer
  T1CONbits.TMR1ON = 0;

  TRISA = 0;
  TRISB = 0;
  TRISC = 0;
  TRISD = 0;
  TRISE = 0;
  PORTA = 0;
  PORTB = 0;
  PORTC = 0;
  PORTD = 0;
  PORTE = 0;

  #if defined(USE_USB_BUS_SENSE_IO)
    tris_usb_bus_sense = INPUT_PIN;
  #endif

  // In the case of a device which can be both self-powered and bus-powered
  // the device must respond correctly to a GetStatus (device) request and
  // tell the host how it is currently powered.
  //
  // To do this you must device a pin which is high when self powered and low
  // when bus powered and define this in HardwareProfile.h
  #if defined(USE_SELF_POWER_SENSE_IO)
  	tris_self_power = INPUT_PIN;
  #endif

  USBOutHandle = 0;
  USBInHandle = 0;

  #if defined(USB_INTERRUPT)
    USBDeviceAttach();
  #endif

  USBDeviceInit();

  RCONbits.IPEN = 1;			//enable priority level interupts
  INTCONbits.GIE_GIEH = 1;	//enable high priority interupts, global interupt enabled
  INTCONbits.TMR0IE = 1;		//overflow interupt enabled
  INTCON2bits.TMR0IP = 1;		//high priority interupt
  PIE1bits.TMR1IE = 1;		//overflow interupt enabled
  IPR1bits.TMR1IP = 0;		//low priority interupt

  // timer 0 bits
  T0CONbits.T08BIT = 0;
  T0CONbits.T0CS = 0;
  T0CONbits.PSA = 0;			// prescaler enabled
  T0CONbits.T0PS2 = 0;
  T0CONbits.T0PS1 = 0;
  T0CONbits.T0PS0 = 1;

  // timer 1 bits
  T1CONbits.RD16 = 0;
  T1CONbits.T1RUN = 1;
  T1CONbits.T1CKPS0 = 1;
  T1CONbits.T1CKPS1 = 1;
  T1CONbits.T1OSCEN = 1;
  T1CONbits.TMR1CS = 0;

  T0CONbits.TMR0ON = 1;
  T1CONbits.TMR1ON = 1;

  PORTBbits.RB3 = 0;
  PORTBbits.RB4 = 0;

  PORTA = 0b00000001;

  // Main processing loop
  while(1)
  {
    #if defined(USB_POLLING)
      // If we are in polling mode the USB device tasks must be processed here
      // (otherwise the interrupt is performing this task)
      USBDeviceTasks();
    #endif

    // Process USB Commands
    processUsbCommands();

  }

}

int lightOn = 0;
int lightPD = 0;
int lightPDLow = 0;
int lightPDHi = 0;
int lightPC = 0;

// Process USB commands
void processUsbCommands(void)
{
  if(USBDeviceState <= ATTACHED_STATE)
  {

  }
  if(USBDeviceState <= POWERED_STATE)
  {
  }

  // Check if we are in the configured state; otherwise just return
  if((USBDeviceState < CONFIGURED_STATE) || (USBSuspendControl == 1))
  {
    // We are not configured
    return;
  }

  if (lightOn) {
    PORTA = 0b00100011;
  } else {
    PORTA = 0b00000011;
  }

	// Check if data was received from the host.
  if(!HIDRxHandleBusy(USBOutHandle))
  {
    switch(ReceivedDataBuffer[0])
	{
      case 0x69:
      {
	      ToSendDataBuffer[0] = 'H';
        ToSendDataBuffer[1] = 'e';
        ToSendDataBuffer[2] = 'l';
        ToSendDataBuffer[3] = 'l';
        ToSendDataBuffer[4] = 'o';

        // Transmit the response to the host
        if(!HIDTxHandleBusy(USBInHandle))
		    {
          USBInHandle = HIDTxPacket(HID_EP,(BYTE*)&ToSendDataBuffer[0],64);
		    }
        break;
      }
      case 0x50:  //P
      {
	    	position = ReceivedDataBuffer[1];
        break;
      }
      case 0x4C:  //L
      {
		    lightOn = ReceivedDataBuffer[1];
        if (lightOn == 1) {
          PORTA = 0b00100011;
        } else {
          PORTA = 0b00000011;
        }
      }
      case 0x4D:
      {
        /*
          lightPD = ReceivedDataBuffer[1];
      		lightPD = lightPD & 0b110011;               // mask port D bits
      		lightPDLow = lightPD << 2 & 0b00111111; // get lower two bits
      		lightPDHi = lightPD & 0b110000;         // get upper two bits
      		lightPD = lightPDLow | lightPDHi;       // combine hi and low bits

      		lightPC = ReceivedDataBuffer[1];
      		lightPC = lightPC & 0b001100;               // mask port C bits
      		lightPC = lightPC << 4;                     // move to correct position

          PORTD = (PORTD & 0b11000011) | lightPD;
          PORTC = (PORTC & 0b00111111) | lightPC;
        */

        // (ports to light     )   buff[1] -> RRGGBB
        // (dim interval       )   buff[2] >> 7 | buff[3]  -> dimInterval
        // (start lights on/off)   buff[4] -> 1 or 0  ON or OFF
        // (counts per dim incr)   buff[5] -> 30?

        dimLightBits = ReceivedDataBuffer[1];
        dimInterval = (ReceivedDataBuffer[2] >> 7) | ReceivedDataBuffer[3];
        dimLightOn = ReceivedDataBuffer[4];
        dimCountPerInc = ReceivedDataBuffer[5];
        dimOff = 0;
        dimIntervalCounter = 0;
        dimTogglePoint = 0;
        dimCounter = 0;
      }

      default:	// Unknown command received
        break;
	}

    // Re-arm the OUT endpoint for the next packet
    USBOutHandle = HIDRxPacket(HID_EP,(BYTE*)&ReceivedDataBuffer,64);
	}
}

// USB Callback handling routines -----------------------------------------------------------

// Call back that is invoked when a USB suspend is detected
void USBCBSuspend(void)
{
}

// This call back is invoked when a wakeup from USB suspend is detected.
void USBCBWakeFromSuspend(void)
{
}

// The USB host sends out a SOF packet to full-speed devices every 1 ms.
void USBCB_SOF_Handler(void)
{
  // No need to clear UIRbits.SOFIF to 0 here. Callback caller is already doing that.
}

// The purpose of this callback is mainly for debugging during development.
// Check UEIR to see which error causes the interrupt.
void USBCBErrorHandler(void)
{
  // No need to clear UEIR to 0 here.
  // Callback caller is already doing that.
}

// Check other requests callback
void USBCBCheckOtherReq(void)
{
  USBCheckHIDRequest();
}

// Callback function is called when a SETUP, bRequest: SET_DESCRIPTOR request arrives.
void USBCBStdSetDscHandler(void)
{
  // You must claim session ownership if supporting this request
}

//This function is called when the device becomes initialized
void USBCBInitEP(void)
{
  // Enable the HID endpoint
  USBEnableEndpoint(HID_EP,USB_IN_ENABLED|USB_OUT_ENABLED|USB_HANDSHAKE_ENABLED|USB_DISALLOW_SETUP);

  // Re-arm the OUT endpoint for the next packet
  USBOutHandle = HIDRxPacket(HID_EP,(BYTE*)&ReceivedDataBuffer,64);
}

// Send resume call-back
void USBCBSendResume(void)
{
  static WORD delay_count;

  // Verify that the host has armed us to perform remote wakeup.
  if(USBGetRemoteWakeupStatus() == FLAG_TRUE)
  {
    // Verify that the USB bus is suspended (before we send remote wakeup signalling).
    if(USBIsBusSuspended() == FLAG_TRUE)
    {
      USBMaskInterrupts();

      // Bring the clock speed up to normal running state
      USBCBWakeFromSuspend();
      USBSuspendControl = 0;
      USBBusIsSuspended = FLAG_FALSE;

      // Section 7.1.7.7 of the USB 2.0 specifications indicates a USB
      // device must continuously see 5ms+ of idle on the bus, before it sends
      // remote wakeup signalling.  One way to be certain that this parameter
      // gets met, is to add a 2ms+ blocking delay here (2ms plus at
      // least 3ms from bus idle to USBIsBusSuspended() == FLAG_TRUE, yeilds
      // 5ms+ total delay since start of idle).
      delay_count = 3600U;
      do
      {
        delay_count--;
      } while(delay_count);

      // Start RESUME signaling for 1-13 ms
      USBResumeControl = 1;
      delay_count = 1800U;
      do
      {
        delay_count--;
      } while(delay_count);
      USBResumeControl = 0;

      USBUnmaskInterrupts();
    }
  }
}

// USB callback function handler
BOOL USER_USB_CALLBACK_EVENT_HANDLER(USB_EVENT event, void *pdata, WORD size)
{
  switch(event)
  {
    case EVENT_TRANSFER:
      // Application callback tasks and functions go here
      break;
    case EVENT_SOF:
      USBCB_SOF_Handler();
      break;
    case EVENT_SUSPEND:
      USBCBSuspend();
      break;
    case EVENT_RESUME:
      USBCBWakeFromSuspend();
      break;
    case EVENT_CONFIGURED:
      USBCBInitEP();
      break;
    case EVENT_SET_DESCRIPTOR:
      USBCBStdSetDscHandler();
      break;
    case EVENT_EP0_REQUEST:
      USBCBCheckOtherReq();
      break;
    case EVENT_BUS_ERROR:
      USBCBErrorHandler();
      break;
    default:
      break;
  }
  return FLAG_TRUE;
}

