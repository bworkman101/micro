#include <SoftwareSerial.h>
#include <Servo.h>

#define RxD 0
#define TxD 1

/**
* Using Board Manager atmega 128.
* As far as I can tell, all atmega 128 board managers work. :)
*  Keep up the good work man.
*/

SoftwareSerial blue(RxD,TxD);
Servo servo1;
Servo servo2;
Servo servo3;
Servo servo4;
Servo servoMotor;

void setup() 
{
  servoMotor.attach(9);
  servoMotor.write(1);
  //armESC(servoMotor);
  
  pinMode(RxD, INPUT);
  pinMode(TxD, OUTPUT);

  servo1.attach(10); // a
  servo2.attach(11); // b
  servo3.attach(6);  // c
  servo4.attach(5);  // d

  servo1.write(90);
  servo2.write(90);
  servo3.write(90);
  servo4.write(90);
}

// The GWS ESCs I'm using have a weird arming sequence:
// move to low throttle, wait a second, then move to zero.
// https://github.com/akkana/arduino/blob/master/motors/ESCmotor/ESCmotor.ino
void armESC(Servo servo)
{
    servo.write(70);
    delay(8000);
    servo.write(1);
}

boolean started = false;
boolean inquiring = false;
char rcvd[] = "    ";
short rcvdCnt = 0;
boolean ctrl = false;

void loop()
{ 
  if(!started) {
    setupBlue();
  } else if (started && !inquiring) {
    inquire();
  }

  blue.overflow(); // not sure what to do if this overflows, but we do need to clear the overflow bit

  while (blue.available() > 0) {
    char ch = blue.read();
    if (ch == 'z') {
      if (!ctrl) {
        ctrl = true;
      }
      else if (ctrl && rcvdCnt == 4) {
        // everything received
        int val = charToNum(rcvd[1]) * 100 + charToNum(rcvd[2]) * 10 + charToNum(rcvd[3]);
        control(rcvd[0], val);
        ctrl = false;
        rcvdCnt = 0;
      }
      else {
        // if received is 0, this could be zz combo
        if (rcvdCnt > 0) {
          // noise, reset everything
          ctrl = false;
          rcvdCnt = 0;
        }
      }
    }
    else if (ctrl) {
      if (rcvdCnt == 4) {
        // noise
        ctrl = false;
        rcvdCnt = 0;
      }
      else {
        rcvd[rcvdCnt] = ch;
      }
      rcvdCnt++;
    }
  }
}

void control(char ctrlChar, int val) {
  switch (ctrlChar) {
    case 'a': {
      servo1.write(val);
      break;
    }
    case 'b': { 
      servo2.write(val);
      break;
    }
    case 'c': { 
      servo3.write(val);
      break;
    }
    case 'd': { 
      servo4.write(val);
      break;
    }
    case 'm': { 
      servoMotor.write(val);
      break;
    }
  }
}

int charToNum(char ch) {
  switch(ch) {
    case '0': return 0;
    case '1': return 1;
    case '2': return 2;
    case '3': return 3;
    case '4': return 4;
    case '5': return 5;
    case '6': return 6;
    case '7': return 7;
    case '8': return 8;
    case '9': return 9;
    default: return 0;
  }
}

void setupBlue()
{
  blue.begin(38400); //Set BluetoothBee BaudRate to default baud rate 38400
  delay(1000);
  sendBlueCmd("\r\n+STWMOD=0\r\n");
  sendBlueCmd("\r\n+STNA=airplane-new\r\n");
  sendBlueCmd("\r\n+STAUTO=0\r\n");
  sendBlueCmd("\r\n+STOAUT=1\r\n");
  sendBlueCmd("\r\n +STPIN=\r\n");
  started = true;
  delay(2000);
}

void inquire() {
  sendBlueCmd("\r\n+INQ=1\r\n");
  inquiring = true;
}

void sendBlueCmd(char command[])
{
    blue.print(command);
    blue.flush();
    //CheckOK();   
}
